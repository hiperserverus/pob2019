import auth from './auth';
import user from './user';
import people from './people';
import peopleTypes from './peopleTypes';
import enterprise from './enterprise';
import department from './department';
import province from './province';
import district from './district';
import construction from './construction';
import protocol from './protocol';
import observationTypes from './observationTypes';
import observation from './observation';
import evaluation from './evaluation';
import option from './option';
import planification from './planification';
import inspection from './inspection';

export default [
  ...auth,
  ...user,
  ...people,
  ...peopleTypes,
  ...enterprise,
  ...department,
  ...province,
  ...district,
  ...construction,
  ...protocol,
  ...observationTypes,
  ...observation,
  ...evaluation,
  ...option,
  ...planification,
  ...inspection,
];
