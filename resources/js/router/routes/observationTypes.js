import requiredAuth from './guards/required-auth';
import List from '@/js/views/observationTypes/List.vue';

export default [
  {
    path: '/observation-type/list',
    name: 'observation-type.list',
    component: List,
    meta: { layout: 'dashboard' },
    beforeEnter: requiredAuth,
  },
];
