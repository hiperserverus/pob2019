import requiredAuth from './guards/required-auth';
import List from '@/js/views/peopleTypes/List.vue';
import Show from '@/js/views/peopleTypes/Show.vue';

export default [
  {
    path: '/people-type/list',
    name: 'people-type.list',
    component: List,
    meta: { layout: 'dashboard' },
    beforeEnter: requiredAuth,
  },
  {
    path: '/people-type/show/:id',
    name: 'people-type.show',
    component: Show,
    meta: { layout: 'dashboard' },
    beforeEnter: requiredAuth,
  },
];
