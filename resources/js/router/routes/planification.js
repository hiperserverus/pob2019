import requiredAuth from './guards/required-auth';
import List from '@/js/views/planification/List.vue';
import Show from '@/js/views/planification/Show.vue';
import Create from '@/js/views/planification/Create.vue';
import Edit from '@/js/views/planification/Edit.vue';

export default [
  {
    path: '/planification/list',
    name: 'planification.list',
    component: List,
    meta: { layout: 'dashboard' },
    beforeEnter: requiredAuth,
  },
  {
    path: '/planification/show/:id',
    name: 'planification.show',
    component: Show,
    meta: { layout: 'dashboard' },
    beforeEnter: requiredAuth,
  },
  {
    path: '/planification/create',
    name: 'planification.create',
    component: Create,
    meta: { layout: 'dashboard' },
    beforeEnter: requiredAuth,
  },
  {
    path: '/planification/edit/:id',
    name: 'planification.edit',
    component: Edit,
    meta: { layout: 'dashboard' },
    beforeEnter: requiredAuth,
  },
];
