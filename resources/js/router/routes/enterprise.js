import requiredAuth from './guards/required-auth';
import List from '@/js/views/enterprise/List.vue';
import Show from '@/js/views/enterprise/Show.vue';
import Create from '@/js/views/enterprise/Create.vue';
import Edit from '@/js/views/enterprise/Edit.vue';

export default [
  {
    path: '/enterprise/list',
    name: 'enterprise.list',
    component: List,
    meta: { layout: 'dashboard' },
    beforeEnter: requiredAuth,
  },
  {
    path: '/enterprise/create',
    name: 'enterprise.create',
    component: Create,
    meta: { layout: 'dashboard' },
    beforeEnter: requiredAuth,
  },
  {
    path: '/enterprise/show/:id',
    name: 'enterprise.show',
    component: Show,
    meta: { layout: 'dashboard' },
    beforeEnter: requiredAuth,
  },
  {
    path: '/enterprise/edit/:id',
    name: 'enterprise.edit',
    component: Edit,
    meta: { layout: 'dashboard' },
    beforeEnter: requiredAuth,
  },
];
