import requiredAuth from './guards/required-auth';
import List from '@/js/views/inspection/List.vue';
import Show from '@/js/views/inspection/Show.vue';

export default [
  {
    path: '/inspection/list',
    name: 'inspection.list',
    component: List,
    meta: { layout: 'dashboard' },
    beforeEnter: requiredAuth,
  },
  {
    path: '/inspection/show/:id',
    name: 'inspection.show',
    component: Show,
    meta: { layout: 'dashboard' },
    beforeEnter: requiredAuth,
  },
];
