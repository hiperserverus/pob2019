import requiredAuth from './guards/required-auth';
import List from '@/js/views/evaluation/List.vue';
import Show from '@/js/views/evaluation/Show.vue';
import Create from '@/js/views/evaluation/Create.vue';
import Edit from '@/js/views/evaluation/Edit.vue';

export default [
  {
    path: '/evaluation/list',
    name: 'evaluation.list',
    component: List,
    meta: { layout: 'dashboard' },
    beforeEnter: requiredAuth,
  },
  {
    path: '/evaluation/show/:id',
    name: 'evaluation.show',
    component: Show,
    meta: { layout: 'dashboard' },
    beforeEnter: requiredAuth,
  },
  {
    path: '/evaluation/create',
    name: 'evaluation.create',
    component: Create,
    meta: { layout: 'dashboard' },
    beforeEnter: requiredAuth,
  },
  {
    path: '/evaluation/edit/:id',
    name: 'evaluation.edit',
    component: Edit,
    meta: { layout: 'dashboard' },
    beforeEnter: requiredAuth,
  },
];
