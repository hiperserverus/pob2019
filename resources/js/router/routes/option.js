import requiredAuth from './guards/required-auth';
import List from '@/js/views/option/List.vue';

export default [
  {
    path: '/option/list',
    name: 'option.list',
    component: List,
    meta: { layout: 'dashboard' },
    beforeEnter: requiredAuth,
  },
];
