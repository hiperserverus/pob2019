import requiredAuth from './guards/required-auth';
import List from '@/js/views/people/List.vue';
import Show from '@/js/views/people/Show.vue';
import Create from '@/js/views/people/Create.vue';
import Edit from '@/js/views/people/Edit.vue';

export default [
  {
    path: '/people/list',
    name: 'people.list',
    component: List,
    meta: { layout: 'dashboard' },
    beforeEnter: requiredAuth,
  },
  {
    path: '/people/show/:id',
    name: 'people.show',
    component: Show,
    meta: { layout: 'dashboard' },
    beforeEnter: requiredAuth,
  },
  {
    path: '/people/create',
    name: 'people.create',
    component: Create,
    meta: { layout: 'dashboard' },
    beforeEnter: requiredAuth,
  },
  {
    path: '/people/edit/:id',
    name: 'people.edit',
    component: Edit,
    meta: { layout: 'dashboard' },
    beforeEnter: requiredAuth,
  },
];
