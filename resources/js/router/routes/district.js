import requiredAuth from './guards/required-auth';
import List from '@/js/views/district/List.vue';

export default [
  {
    path: '/district/list',
    name: 'district.list',
    component: List,
    meta: { layout: 'dashboard' },
    beforeEnter: requiredAuth,
  },
];
