import requiredGuest from './guards/required-guest';
import Register from '@/js/views/auth/Register.vue';
import Login from '@/js/views/auth/Login.vue';

export default [
  {
    path: '/auth/register',
    name: 'register',
    component: Register,
    meta: { layout: 'no-dashboard' },
    beforeEnter: requiredGuest,
  },
  {
    path: '',
    name: 'login',
    component: Login,
    meta: { layout: 'no-dashboard' },
    beforeEnter: requiredGuest,
  },
];
