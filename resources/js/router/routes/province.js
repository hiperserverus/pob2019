import requiredAuth from './guards/required-auth';
import List from '@/js/views/province/List.vue';

export default [
  {
    path: '/province/list',
    name: 'province.list',
    component: List,
    meta: { layout: 'dashboard' },
    beforeEnter: requiredAuth,
  },
];
