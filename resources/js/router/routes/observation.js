import requiredAuth from './guards/required-auth';
import List from '@/js/views/observation/List.vue';

export default [
  {
    path: '/observation/list',
    name: 'observation.list',
    component: List,
    meta: { layout: 'dashboard' },
    beforeEnter: requiredAuth,
  },
];
