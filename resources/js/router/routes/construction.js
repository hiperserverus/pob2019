import requiredAuth from './guards/required-auth';
import List from '@/js/views/construction/List.vue';
import Show from '@/js/views/construction/Show.vue';
import Create from '@/js/views/construction/Create.vue';
import Edit from '@/js/views/construction/Edit.vue';

export default [
  {
    path: '/construction/list',
    name: 'construction.list',
    component: List,
    meta: { layout: 'dashboard' },
    beforeEnter: requiredAuth,
  },
  {
    path: '/construction/show/:id',
    name: 'construction.show',
    component: Show,
    meta: { layout: 'dashboard' },
    beforeEnter: requiredAuth,
  },
  {
    path: '/construction/create',
    name: 'construction.create',
    component: Create,
    meta: { layout: 'dashboard' },
    beforeEnter: requiredAuth,
  },
  {
    path: '/construction/edit/:id',
    name: 'construction.edit',
    component: Edit,
    meta: { layout: 'dashboard' },
    beforeEnter: requiredAuth,
  },
];
