import requiredAuth from './guards/required-auth';
import List from '@/js/views/department/List.vue';
import Show from '@/js/views/department/Show.vue';

export default [
  {
    path: '/department/list',
    name: 'department.list',
    component: List,
    meta: { layout: 'dashboard' },
    beforeEnter: requiredAuth,
  },
  {
    path: '/department/show/:id',
    name: 'department.show',
    component: Show,
    meta: { layout: 'dashboard' },
    beforeEnter: requiredAuth,
  },
];
