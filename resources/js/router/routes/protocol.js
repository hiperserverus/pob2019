import requiredAuth from './guards/required-auth';
import List from '@/js/views/protocol/List.vue';

export default [
  {
    path: '/protocol/list',
    name: 'protocol.list',
    component: List,
    meta: { layout: 'dashboard' },
    beforeEnter: requiredAuth,
  },
];
