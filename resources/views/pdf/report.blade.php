<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

  <title>Informe de supervición</title>
  {{-- Icon --}}
  <link rel="icon" href="{{asset('img/logo-redondo.png')}}">
</head>
<body>
  <header>
    <div class="container">
      <div class="row" id="header">
        <div class="col-6 pt-4 pb-3 text-center border-bottom border-dark ">
          <h6 class="m-0"><strong>COMPAÑIA MINERA LUREN S.A.</strong></h6>
          <p class="m-0 font-header">Línea de Placas y Ladrillo King Kong 11H Sílico Calcáreos</p>
          <p class="m-0 font-header">Morteros y Concretos Embolsados</p>
          <p class="m-0 font-header">Adoquines y Lajas de Concreto-Piedra</p>
          <p class="m-0 font-header">Cal Viva y Cal Hidratada – Carbonato de Calcio</p>
        </div>
        <div class="col-4 text-center ml-auto">
          <img src="{{ public_path('images/logo-lacasa.jpg') }}" width="300px" height="150px">
        </div>
      </div>
    </div>
  </header>

  <footer class="border-top border-dark">
    <div class="mt-2">
      <p class="font-header text-center m-0">Ventas y Asesoría Técnica: Av. Petit Thouars 5056 Miraflores - 242-3637 – Email: <a href="lacasaventas@mineraluren.com.pe">lacasaventas@mineraluren.com.pe</a></p>
      <p class="font-header text-center m-0">Planta: Antigua Panamericana Sur Km.23.5 V.E.S. - 295-7054 - 295-7056 – Email: <a href=" planta@mineraluren.com.pe"> planta@mineraluren.com.pe</a></p>
      <p class="font-header text-center m-0">Web: <a href="www.lacasa.com.pe">www.lacasa.com.pe</a></p>
    </div>
  </footer>

  <main>
    <div id="content" class="padding-content mx-5">
      <p class="text-right"> {{ $today }} </p>
      <h6>Señores:</h6>
      <h6><strong> {{ $enterprise }} </strong></h6>
      <h6>Atención:</h6>
      @foreach ($inspection->peoples as $people)
          @if ($people->pivot->leader === 0)
            <h6 class="d-inline"><strong> {{ $people->first_name }} {{ $people->last_name }} </strong></h6>
          @endif
      @endforeach

      <p class="text-justify mt-2">Por la presente nos es muy gratos saludarlos y a la vez alcanzarles los resultados de la verificación realizada el día {{ $day }} de {{ $month }}, con la ficha de supervisión n° {{ sprintf("%04s", $inspection->planification->construction->id) }} sobre el procedimiento de instalación de nuestros materiales que viene realizando el instalador <strong>{{ $installer }}</strong>, en la obra "{{ $inspection->planification->construction->description }}" ubicada en {{ $inspection->planification->construction->district->name }}, en la que hemos observado los siguientes puntos: </p>

      @if ($observationTypeAvailable[0] === 1)
        <h6 class="text-justify mt-3 mb-3"><strong> 1.- OBSERVACIONES GRAVES: </strong> Aquellas que atentan contra la seguridad y debido comportamiento sísmico del muro y que deben ser solucionadas en forma efectiva e inmediata:</h6>

        @for ($i = 0; $i < count($observationGAvailable); $i++)

          <h6 class="text-justify"><strong> 1.{{ $i+1 }}.- {{$inspection->reports[$i]->observation->description}} </strong></h6>

          @foreach ($inspection->reports as $report)
            <ul>
              @if ($observationGAvailable[$i] === $report->evaluation->observation_id)
                <li class="text-justify pb-2">
                  {{$report->evaluation->description}}
                  <ul>
                    <li class="text-justify pb-2">
                      {{$report->option->description}}
                    </li>
                  </ul>
                </li>
                @if ($report->urlImage !== null)
                  @foreach ($report->images as $image)
                    @if (file_exists('storage/report/'. $report->id . '-report/image/' . $image->path))
                      <div class="text-center mt-5">
                        <img src="{{public_path('storage/report/'. $report->id . '-report/image/' . $image->path)}}" width="400px" height="400px" alt="">
                      </div>
                    @endif
                  @endforeach
                @endif
              @endif
            </ul>
          @endforeach
        @endfor

      @else
        <h6 class="text-justify mt-3 mb-3"><strong> 1.- OBSERVACIONES SECUNDARIAS: </strong> Aquellas que no necesariamente atentan contra la seguridad y comportamiento sísmico del muro pero que deberían ser corregidas para asegurar el debido proceso constructivo que nos garantice la calidad integral del muro:</h6>
        @for ($i = 0; $i < count($observationSAvailable); $i++)

          <h6 class="text-justify"><strong> 1.{{ $i+1 }}.- {{$inspection->reports[$i]->observation->description}} </strong></h6>

          @foreach ($inspection->reports as $report)
            <ul>
              @if ($observationSAvailable[$i] === $report->evaluation->observation_id)
                <li class="text-justify pb-2">
                  {{$report->evaluation->description}}
                  <ul>
                    <li class="text-justify pb-2">
                      {{$report->option->description}}
                    </li>
                  </ul>
                </li>
                @if ($report->urlImage !== null)
                  @foreach ($report->images as $image)
                    @if (file_exists('storage/report/'. $report->id . '-report/image/' . $image->path))
                      <div class="text-center mt-5">
                        <img src="{{public_path('storage/report/'. $report->id . '-report/image/' . $image->path)}}" width="400px" height="400px" alt="">
                      </div>
                    @endif
                  @endforeach
                @endif
              @endif
            </ul>
          @endforeach

        @endfor
      @endif

      @if (isset($observationTypeAvailable[1]) === true)
        @if ($observationTypeAvailable[1] === 2)
          <h6 class="text-justify mt-3 mb-3"><strong> 2.- OBSERVACIONES SECUNDARIAS: </strong> Aquellas que no necesariamente atentan contra la seguridad y comportamiento sísmico del muro pero que deberían ser corregidas para asegurar el debido proceso constructivo que nos garantice la calidad integral del muro:</h6>
          @for ($i = 0; $i < count($observationSAvailable); $i++)

            <h6 class="text-justify"><strong> 2.{{ $i+1 }}.- {{$inspection->reports[$i]->observation->description}} </strong></h6>

            @foreach ($inspection->reports as $report)
              <ul>
                @if ($observationSAvailable[$i] === $report->evaluation->observation_id)
                  <li class="text-justify pb-2">
                    {{$report->evaluation->description}}
                    <ul>
                      <li class="text-justify pb-2">
                        {{$report->option->description}}
                      </li>
                    </ul>
                  </li>
                  @if ($report->urlImage !== null)
                    @foreach ($report->images as $image)
                      @if (file_exists('storage/report/'. $report->id . '-report/image/' . $image->path))
                        <div class="text-center mt-5">
                          <img src="{{public_path('storage/report/'. $report->id . '-report/image/' . $image->path)}}" width="400px" height="400px" alt="">
                        </div>
                      @endif
                    @endforeach
                  @endif
                @endif
              </ul>
            @endforeach

          @endfor

        @else

          <h6 class="text-justify mt-3 mb-3"><strong> 2.- OBSERVACIONES GRAVES: </strong> Aquellas que atentan contra la seguridad y debido comportamiento sísmico del muro y que deben ser solucionadas en forma efectiva e inmediata:</h6>

          @for ($i = 0; $i < count($observationGAvailable); $i++)

            <h6 class="text-justify"><strong> 2.{{ $i+1 }}.- {{$inspection->reports[$i]->observation->description}} </strong></h6>

            @foreach ($inspection->reports as $report)
              <ul>
                @if ($observationGAvailable[$i] === $report->evaluation->observation_id)
                  <li class="text-justify pb-2">
                    {{$report->evaluation->description}}
                    <ul>
                      <li class="text-justify pb-2">
                        {{$report->option->description}}
                      </li>
                    </ul>
                  </li>
                  @if ($report->urlImage !== null)
                    @foreach ($report->images as $image)
                      @if (file_exists('storage/report/'. $report->id . '-report/image/' . $image->path))
                        <div class="text-center mt-5">
                          <img src="{{public_path('storage/report/'. $report->id . '-report/image/' . $image->path)}}" width="400px" height="400px" alt="">
                        </div>
                      @endif
                    @endforeach
                  @endif
                @endif
              </ul>
            @endforeach

          @endfor
        @endif
      @endif

    </div>
  </main>

</body>
</html>

<style type="text/css" media="all">
  @page {
    margin: 200px 25px 150px 25px;
  }
  header {
    position: fixed;
    top: -200px;
    left: 0px;
    right: 0px;
    height: 50px;
  }
  footer {
    position: fixed;
    bottom: -120px;
    left: 0px;
    right: 0px;
    height: 40px;
  }
  .font-header {
    font-size: 10px;
  }
  .padding-content {
    margin-top: 10px;
  }
</style>
