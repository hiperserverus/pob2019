<?php

use Illuminate\Http\Request;

$defaultResources = ['index', 'show', 'store', 'update', 'destroy'];

Route::group([
    'middleware' => 'api',
    'prefix' => 'auth'
], function ($router) {
    Route::post('login', 'AuthController@login');
    Route::post('login-app', 'AuthController@loginApp');
    Route::post('register', 'AuthController@register');
    Route::post('logout', 'AuthController@logout');
    Route::post('refresh', 'AuthController@refresh');
    Route::post('user', 'AuthController@user');
    Route::post('recover-password/{email}', 'AuthController@forgotPassword');
    Route::post('/register-app', 'PeopleController@appRegister');
    Route::post('/edit-profile/{id}', 'PeopleController@updateProfile');
    Route::post('/edit-profile-photo/{id}', 'PeopleController@updateProfilePhoto');
});

Route::group([
    'middleware' => 'api'
], function() use ($defaultResources){

    /**
     * User Resource Routes
     */

    Route::resource('user', 'UserController', [
        'only' => $defaultResources
    ]);

    /**
     * People Resource Routes
     */

    Route::resource('people', 'PeopleController', [
        'only' => $defaultResources
    ]);

    /**
     * People Types Resource Routes
     */

    Route::resource('people-types', 'PeopleTypeController', [
        'only' => $defaultResources
    ]);

    /**
     * Enterprise Resource Routes
     */

    Route::resource('enterprise', 'EnterpriseController', [
        'only' => $defaultResources
    ]);
    Route::get('enterprise-app', 'EnterpriseController@indexAPP');
    Route::get('enterprise-construction', 'EnterpriseController@constructionEnterprise');

    /**
     * Ubigeo Departments
     */
    Route::resource('department', 'DepartmentController', [
        'only' => $defaultResources
    ]);

    /**
     * Ubigeo Provinces
     */
    Route::resource('province', 'ProvinceController', [
        'only' => $defaultResources
    ]);

    /**
     * Ubigeo Districts
     */
    Route::resource('district', 'DistrictController', [
        'only' => $defaultResources
    ]);

    /**
     * People Custom Routes
     */

    Route::group([
        'prefix' => 'people'
    ], function ($router) {
        Route::get('/constructions/{id}', 'PeopleController@constructionPerUser');
        Route::get('/planifications/{id}', 'PeopleController@planificationPerUser');
        Route::get('/inspections/{id}', 'PeopleController@inspectionPerUser');
        Route::get('/constructions-supervisor/{id}', 'PeopleController@allData');
        Route::get('/confirm/{id}', 'PeopleController@changeStatus');
    });

    /**
     * Construction Resource Routes
     */

    Route::resource('construction', 'ConstructionController', [
        'only' => $defaultResources
    ]);

    /**
     * Construction Custom Routes
     */

    Route::group([
        'prefix' => 'construction'
    ], function ($router) {
        Route::get('/confirm/{id}', 'ConstructionController@changeStatus');
    });

    /**
     * Protocol Resource Routes
     */

    Route::resource('protocol', 'ProtocolController', [
        'only' => $defaultResources
    ]);

    /**
     * Protocol Custom Routes
     */

    Route::group([
        'prefix' => 'protocol'
    ], function ($router) {
        Route::get('/duplicate/{id}', 'ProtocolController@copy');
    });

    /**
     * Observation Types Resource Routes
     */

    Route::resource('observation-types', 'ObservationTypeController', [
        'only' => $defaultResources
    ]);

    /**
     * Observation Resource Routes
     */

    Route::resource('observation', 'ObservationController', [
        'only' => $defaultResources
    ]);

    /**
     * Evaluation Resource Routes
     */

    Route::resource('evaluation', 'EvaluationController', [
        'only' => $defaultResources
    ]);

    /**
     * Option Resource Routes
     */

    Route::resource('option', 'OptionController', [
        'only' => $defaultResources
    ]);

    /**
     * Planification Resource Routes
     */

    Route::resource('planification', 'PlanificationController', [
        'only' => $defaultResources
    ]);

    /**
     * Inspection Resource Routes
     */

    Route::resource('inspection', 'InspectionController', [
        'only' => $defaultResources
    ]);

    Route::get('pdf/{id}', 'InspectionController@pdf');
    Route::post('validate-location', 'InspectionController@validateLocation');

    /**
     * Report Resource Routes
     */

    Route::resource('report', 'ReportController', [
        'only' => ['index', 'store']
    ]);

    Route::post('/report/{id}', 'ReportController@update');
    Route::put('/finish-planification/{id}', 'ReportController@finishPlanification');
});
