<?php

use Illuminate\Database\Seeder;
use App\Model\Department;
use Carbon\Carbon;

class DepartmentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $departments = [
            'Amazonas',
            'Ancash',
            'Apurimac',
            'Arequipa',
            'Ayacucho',
            'Cajamarca',
            'Callao',
            'Cusco',
            'Huancavelica',
            'Huanuco',
            'Ica',
            'Junin',
            'La Libertad',
            'Lambayeque',
            'Lima',
            'Loreti',
            'Madre de Dios',
            'Moquegua',
            'Pasco',
            'Piura',
            'Puno',
            'San Martin',
            'Tacna',
            'Tumbes',
            'Ucayali'
        ];

        for ($i=0; $i < count($departments); $i++) {
            $department = new Department();
            $department->name = $departments[$i];
            $department->code = sprintf("%02d", $i + 1);
            $department->created_at = Carbon::now()->format('Y-d-m H:i:s');
            $department->updated_at = Carbon::now()->format('Y-d-m H:i:s');
            $department->save();
        }
    }
}
