<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->command->call('migrate:fresh');
        $this->call(UsersTableSeeder::class);
        /*$this->call(PeopleTypesTableSeeder::class);
        $this->call(DepartmentsTableSeeder::class);
        $this->call(ProvincesTableSeeder::class);
        $this->call(DistrictsTableSeeder::class);
        $this->call(EnterprisesTableSeeder::class);*/
        //$this->call(PeopleTableSeeder::class);
        //$this->call(ConstructionsTableSeeder::class);
        //$this->call(ProtocolsTableSeeder::class);
        //$this->call(ObservationTypesTableSeeder::class);
        //$this->call(ObservationsTableSeeder::class);
        //$this->call(OptionsTableSeeder::class);
        //$this->call(EvaluationsTableSeeder::class);
        //$this->call(PlanificationsTableSeeder::class);
    }
}
