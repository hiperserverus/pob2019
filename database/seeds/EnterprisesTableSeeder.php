<?php

use Illuminate\Database\Seeder;
use App\Model\Enterprise;
use App\Model\PeopleType;
use Carbon\Carbon;

class EnterprisesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $enterprises = [
            'Mineraluren',
            'Independiente'
        ];

        foreach ($enterprises as $e ) {
            $enterprise = new Enterprise();
            $enterprise->department_id = 1;
            $enterprise->province_id = 1;
            $enterprise->district_id = 1;
            $enterprise->name = $e;
            $enterprise->auxiliaryCode = '01';
            $enterprise->auxiliaryName = 'unico';
            $enterprise->origin = 'Nacional';
            $enterprise->auxiliarySorter = 'Trabajador';
            $enterprise->documentType = 'RUC';
            $enterprise->ruc = '01010101';
            $enterprise->address = 'Sin ubicación';
            $enterprise->ubigeo = '010101';
            $enterprise->location = 'Sin localidad';
            $enterprise->contactName = 'Sin nombre de contacto';
            $enterprise->phone = '01010101';
            $enterprise->fax = '0101010101';
            $enterprise->email = $e . '@email.com';
            $enterprise->status = 'Activo';
            $enterprise->created_at = Carbon::now()->format('Y-d-m H:i:s');
            $enterprise->updated_at = Carbon::now()->format('Y-d-m H:i:s');
            $enterprise->save();
        }

        $enterprises = Enterprise::get();
        $peopleTypes = PeopleType::get();

        foreach ($enterprises as $enterprise) {
            foreach ($peopleTypes as $peopleType) {
                $enterprise->peopleTypes()->attach($peopleType->id);
            }
        }
    }
}
