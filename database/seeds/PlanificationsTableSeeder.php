<?php

use Illuminate\Database\Seeder;
use App\Model\Planification;
use App\Model\People;
use App\Model\PeopleType;
use Carbon\Carbon;

class PlanificationsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i=0; $i < 15; $i++) {
            $planification = new Planification();
            $planification->construction_id = ((int)$i+1);
            $planification->protocol_id = 1;
            $planification->description = 'Obra programada N°'. ((int)$i+1);
            $planification->start_at = Carbon::now();
            $planification->end_at = Carbon::tomorrow();
            $planification->status = 'Asignado';
            $planification->created_at = Carbon::now()->format('Y-d-m H:i:s');
            $planification->updated_at = Carbon::now()->format('Y-d-m H:i:s');
            $planification->save();
        }
        $planifications = Planification::get();
        $peoples = People::get();
        $peopleType = PeopleType::get();

        foreach ($planifications as $planification) {
            for ($i=1; $i < 7; $i++) {
                $planification->peoples()->attach($peoples[$i], [
                    'people_type_id' => $peopleType[$i]->id,
                    'leader' => $i === 1 ? 1 : 0,
                    ]);
            }
        }
    }
}
