<?php

use Illuminate\Database\Seeder;
use App\Model\Evaluation;
use App\Model\Option;
use Illuminate\Support\Str;
use Carbon\Carbon;

class EvaluationsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $evals = [
            1 => [
                'No se está respetando la ejecución de los trabajos preliminares como es el tarrajeo del cielo raso, vigas y columnas.',
                'No se están realizando los trabajos de ejecución de las instalaciones eléctricas y sanitarias que deben ser armadas y pegadas por el personal eléctrico o sanitario antes de iniciar el asentado del muro.',
                'Las actividades de anclaje son preliminares. Esto quiere decir que el asentado de las unidades no deberá iniciarse hasta que se haya concluido con todos los puntos del procedimiento de anclaje de las varillas verticales Y anclajes de ojo chino, así como el correcto fraguado del adhesivo epóxico (mínimo 24hrs).'
            ],
            2 => [
                'No se está realizando el trazo del eje del muro en la losa superior para una correcta ubicación y alineamiento de los anclajes tipo puente.',
                'No se está respetando el uso de los pernos expansivos para la fijación del anclaje tipo puente.',
                'No se está realizando una correcta fijación de los pernos expansivos, para la fijación del anclaje tipo puente.'
            ],
            3 => [
                'No se está realizando la correcta modulación del acero vertical cada 51 cm. en alfeizar o parapetos.',
                'No se está respetando el diámetro de las perforaciones para los anclajes de las varillas verticales. Dicho diámetro debe estar de acuerdo con las recomendaciones de cada fabricante de pegamento epóxico, tal y como se muestra en el ANEXO I de la página 27 de nuestro manual de instalación (decía los diámetros para SIKADUR 31 y lo hemos cambiado).',
                'No se está respetando la profundidad de las perforaciones para el anclaje de los aceros verticales, según el diámetro de refuerzo en los alfeizar o parapetos.'
            ],
            4 => [
                'No se está respetando el espesor del aislamiento entre el muro y la estructura especificada en los planos del proyecto.',
                'No se está realizando una correcta colocación del teknoport entre el muro y la estructura.',
                'No se está realizando una correcta colocación del teknoport, en las juntas verticales entre muro y la estructura.'
            ],
            5 => [
                'No se está utilizando el tipo de placa según el tiempo de resistencia al fuego, indicado en los planos del proyecto.',
                'No se está realizando el correcto llenado de todos los alveolos en Placa P-10 para soportar el tiempo mínimo de 2 horas de resistencia al fuego.',
                'No se está respetando la aplicación del sellador corta fuego en las juntas entre el muro y la estructura.'
            ],
            6 => [
                'No se está respetando el uso correcto de nuestros productos embolsados.',
                'No se está respetando el espesor de las juntas verticales de mortero grueso en el asentado de las unidades, el cual deberá ser de 1.0 cm. con una tolerancia ± 2 mm.',
                'No se está respetando el espesor de las juntas horizontales de mortero grueso en el asentado de las unidades, el cual deberá ser de 1.5 cm. con una tolerancia ± 3 mm.'
            ],
            7 => [
                'No se está respetando el espesor de las juntas verticales de mortero grueso en el asentado de las unidades, el cual deberá ser de 1.0 cm. con una tolerancia ± 2 mm.',
                'No se está respetando el espesor de las juntas horizontales de mortero grueso en el asentado de las unidades, el cual deberá ser de 1.5 cm. con una tolerancia ± 3 mm.',
                'No se está realizando un correcto relleno de las juntas verticales con mortero grueso entre las unidades.'
            ],
            8 => [
                'No se está respetando el tiempo de secado de los muros antes del inicio del empaste.',
                'Los trabajos ejecutados no permiten realizar observaciones al proceso constructivo.',
                'No se encontraron observaciones durante la inspección.'
            ]
        ];

        $i = 1;

        foreach ($evals as $id => $values) {
            foreach ($values as $value) {
                $evaluation = new Evaluation();
                $evaluation->observation_id = $id;
                $evaluation->description = $value;
                $evaluation->short_description = 'Descripción corta '. $i++;
                $evaluation->status = 'Activo';
                $evaluation->percentage = rand(10,50);
                $evaluation->created_at = Carbon::now()->format('Y-d-m H:i:s');
                $evaluation->updated_at = Carbon::now()->format('Y-d-m H:i:s');
                $evaluation->save();
            }
        }

        $evaluations = Evaluation::get();
        $options = Option::get();

        foreach ($evaluations as $evaluation ) {
            for ($i=0; $i < 3; $i++) {
                $evaluation->options()->attach($options[$i], ['percentage' => 10, 'defaultOption' => $i === 0 ? true : false]);
            }
        }
    }
}
