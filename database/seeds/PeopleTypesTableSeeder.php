<?php

use Illuminate\Database\Seeder;
use App\Model\PeopleType;
use Carbon\Carbon;

class PeopleTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $types = [
            'Supervisor interno - Propio',
            'Supervisor interno - Contratado',
            'Supervisor externo - Cliente',
            'Supervidor externo - Tercero',
            'Vendedor',
            'Capataz',
            'Ingeniero residente',
            'Contratista',
            'Propietario'
        ];

        $supervisor = [1, 1, 1, 1, 0, 0, 0, 0, 0];
        $internal = [1, 1, 0, 0, 1, 0, 0, 0, 0];

        for ($i=0; $i < count($types); $i++)
        {
            $person = new PeopleType();
            $person->name = $types[$i];
            $person->isSupervisor = $supervisor[$i];
            $person->isInternal = $internal[$i];
            $person->created_at = Carbon::now()->format('Y-d-m H:i:s');
            $person->updated_at = Carbon::now()->format('Y-d-m H:i:s');
            $person->save();
        }
    }
}
