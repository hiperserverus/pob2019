<?php

use Illuminate\Database\Seeder;
use App\Model\Option;
use Carbon\Carbon;

class OptionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $options = [
            'Esta observación se detectó en algunos muros.',
            'Esta observación se detectó en todos los muros.',
            'Esta observación se detectó en todos los muros laterales.',
            'Esta observación se detectó en todos los muros delanteros.',
            'Esta observación se detectó en todos los muros traseros.',
            'Sin opciones.'
        ];

        foreach ($options as $option) {
            $op = new Option();
            $op->description = $option;
            $op->status = 'Activo';
            $op->created_at = Carbon::now()->format('Y-d-m H:i:s');
            $op->updated_at = Carbon::now()->format('Y-d-m H:i:s');
            $op->save();
        }
    }
}
