<?php

use Illuminate\Database\Seeder;
use App\Model\People;
use App\Model\PeopleType;
use Carbon\Carbon;

class PeopleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /* $first_name = [
            'Chicky',
            'Enrique',
            'María',
            'Ricardo',
            'Ramón',
            'Héctor',
            'Mario',
            'Miguel',
            'Jessica',
            'Andrea',
            'Franibe',
            'Johangel',
            'John',
            'Gabriel',
            'Masiel',
            'Carlos'
        ];
        $last_name = [
            'Ng',
            'Bermúdez',
            'Centeno',
            'Campos',
            'Rivas',
            'Acuña',
            'Alexio',
            'Soto',
            'Torres',
            'Alvarado',
            'Rodríguez',
            'León',
            'Martinez',
            'Reyes',
            'Silva',
            'Arias'
        ];

        $email = [
            'admin@email.com',
            'e.bermudez@email.com',
            'm.centeno@email.com',
            'r.campos@email.com',
            'r.rivas@email.com',
            'h.acuña@email.com',
            'm.alexio@email.com',
            'm.soto@email.com',
            'j.torres@email.com',
            'a.alvarado@email.com',
            'f.rodriguez@email.com',
            'j.leon@email.com',
            'j.martinez@email.com',
            'g.reyes@email.com',
            'm.silva@email.com',
            'c.arias@email.com',
        ];

        $phone = [
            '+58 4124901514',
            '+58 4124901514',
            '+58 4124312114',
            '+58 4124764514',
            '+58 4124946376',
            '+58 4124946546',
            '+58 4128765121',
            '+58 4128713456',
            '+58 4127897951',
            '+58 4129787911',
            '+58 4121325468',
            '+58 4124654621',
            '+58 4142132546',
            '+58 4124564563',
            '+58 4129654421',
            '+58 4128127391',
        ];

        for ($i=0; $i < count($email); $i++) {
            $people = new People();
            $people->enterprise_id = rand(1,7);
            $people->user_id = $i+1;
            $people->first_name = $first_name[$i];
            $people->last_name = $last_name[$i];
            $people->email = $email[$i];
            $people->phone = $phone[$i];
            $people->address = 'Ubicación'.$i;
            $people->type = 'Interno';
            $people->rut = rand(11111111,99999999);
            $people->birthdate = '1995-09-15';
            $people->status = 'Confirmado';
            $people->created_at = Carbon::now()->format('Y-d-m H:i:s');
            $people->updated_at = Carbon::now()->format('Y-d-m H:i:s');
            $people->save();
        } */

        $people = new People();
        $people->enterprise_id = 1;
        $people->user_id = 1;
        $people->first_name = 'Minera';
        $people->last_name = 'Luren';
        $people->email = 'admin@mineraluren.com';
        $people->phone = '+58 4124901514';
        $people->address = 'Ubicación';
        $people->type = 'Interno';
        $people->rut = rand(11111111,99999999);
        $people->birthdate = '1995-09-15';
        $people->status = 'Confirmado';
        $people->created_at = Carbon::now()->format('Y-d-m H:i:s');
        $people->updated_at = Carbon::now()->format('Y-d-m H:i:s');
        $people->save();

        $peoples = People::get();
        $peopleTypes = PeopleType::get();

        foreach ($peoples as $person) {
            foreach ($peopleTypes as $peopleType) {
                $person->peopleTypes()->attach($peopleType->id);
            }
        }
    }
}
