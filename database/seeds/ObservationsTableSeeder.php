<?php

use Illuminate\Database\Seeder;
use App\Model\Observation;
use Illuminate\Support\Str;
use Carbon\Carbon;

class ObservationsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $observacionGrave = [
            'Anclajes de las varillas verticales con epóxico en las zonas donde las varillas caen en elementos estructurales de concreto.',
            'Anclajes de las varillas verticales en las zonas donde las varillas caen en ladrillos de techo en caso de losas aligeradas.',
            'Anclajes de las varillas verticales con epóxico en alfeizares.',
            'Aislamiento de los muros.',
            'Muros Corta Fuego.'
        ];
        $observacionSecundaria = [
            'Construcción del muro.',
            'Alfeizares y parapetos.',
            'Empaste de los muros.'
        ];

        $i = 1;

        foreach ($observacionGrave as $oG ) {
            $observation = new Observation();
            $observation->protocol_id = 1;
            $observation->observation_type_id = 1;
            $observation->description = $oG;
            $observation->short_description = 'Descripción corta '. $i++;
            $observation->created_at = Carbon::now()->format('Y-d-m H:i:s');
            $observation->updated_at = Carbon::now()->format('Y-d-m H:i:s');
            $observation->save();
        }

        foreach ($observacionSecundaria as $oS ) {
            $observation = new Observation();
            $observation->protocol_id = 1;
            $observation->observation_type_id = 2;
            $observation->description = $oS;
            $observation->short_description = 'Descripción corta '. $i++;
            $observation->created_at = Carbon::now()->format('Y-d-m H:i:s');
            $observation->updated_at = Carbon::now()->format('Y-d-m H:i:s');
            $observation->save();
        }
    }
}
