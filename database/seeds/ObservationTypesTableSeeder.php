<?php

use Illuminate\Database\Seeder;
use App\Model\ObservationType;
use Carbon\Carbon;

class ObservationTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $observatioType = ['Observación grave', 'Observación secundaria'];

        foreach($observatioType as $o)
        {
            $ot = new ObservationType();
            $ot->name = $o;
            $ot->created_at = Carbon::now()->format('Y-d-m H:i:s');
            $ot->updated_at = Carbon::now()->format('Y-d-m H:i:s');
            $ot->save();
        }
    }
}
