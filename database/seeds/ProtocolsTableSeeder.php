<?php

use Illuminate\Database\Seeder;
use App\Model\Protocol;
use Carbon\Carbon;

class ProtocolsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $protocol = new Protocol();
        $protocol->name = 'Protocolo de supervisión de obra';
        $protocol->status = 'Activo';
        $protocol->created_at = Carbon::now()->format('Y-d-m H:i:s');
        $protocol->updated_at = Carbon::now()->format('Y-d-m H:i:s');
        $protocol->save();
    }
}
