<?php

use Illuminate\Database\Seeder;
use App\Model\Construction;
use App\Model\People;
use App\Model\PeopleType;
use Carbon\Carbon;

class ConstructionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i=0; $i < 16; $i++) {
            $construction = new Construction();
            $construction->enterprise_id = rand(1,7);
            $construction->department_id = 1;
            $construction->province_id = 1;
            $construction->district_id = 1;
            $construction->description = 'Obra de prueba N°' . ((int)$i+1);
            $construction->address = 'Dirección de prueba '. ((int)$i+1);
            $construction->ubigeo = '010101';
            $construction->latitude = '-12.0431800';
            $construction->longitude = '-77.0282400';
            $construction->zip_code = '15010';
            $construction->start_date = Carbon::now();
            $construction->end_date = Carbon::now()->addMonth(2);
            $construction->status = 'Confirmada';
            $construction->hasPlanification = 1;
            $construction->created_at = Carbon::now()->format('Y-d-m H:i:s');
            $construction->updated_at = Carbon::now()->format('Y-d-m H:i:s');
            $construction->save();
        }

        $constructions = Construction::get();
        $peoples = People::get();
        $peopleType = PeopleType::get();

        foreach ($constructions as $c) {
            for ($i=1; $i < 7; $i++) {
                $c->peoples()->attach($peoples[$i], [
                    'people_type_id' => $peopleType[$i]->id,
                    'leader' => $i === 1 ? 1 : 0,
                    ]);
            }
        }
    }
}
