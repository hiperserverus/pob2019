<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reports', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->unsignedBigInteger('inspection_id');
            $table->unsignedBigInteger('observation_id')->nullable();
            $table->unsignedBigInteger('evaluation_id')->nullable();
            $table->unsignedBigInteger('option_id')->nullable();
            $table->longText('comment')->nullable();
            $table->longText('urlImage')->nullable();
            $table->longText('urlVideo')->nullable();
            $table->integer('evaluation_percentage')->nullable();
            $table->integer('evaluation_option_percentage')->nullable();

            $table->timestamps(2);

            $table->foreign('inspection_id')->references('id')->on('inspections')/* ->onDelete('cascade') */;
            $table->foreign('observation_id')->references('id')->on('observations')/* ->onDelete('cascade') */;
            $table->foreign('evaluation_id')->references('id')->on('evaluations')/* ->onDelete('cascade') */;
            $table->foreign('option_id')->references('id')->on('options')/* ->onDelete('cascade') */;
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reports');
    }
}
