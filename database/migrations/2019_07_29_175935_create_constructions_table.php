<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConstructionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('constructions', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->unsignedBigInteger('enterprise_id');
            $table->unsignedBigInteger('department_id');
            $table->unsignedBigInteger('province_id');
            $table->unsignedBigInteger('district_id');
            $table->string('auxiliaryCode')->nullable();
            $table->string('auxiliaryConstruction')->nullable();
            $table->longText('description');
            $table->longText('address');
            $table->string('ubigeo');
            $table->longText('latitude');
            $table->longText('longitude');
            $table->string('zip_code')->nullable();
            $table->string('start_date');
            $table->string('end_date');
            $table->string('type')->default('Interno');
            $table->boolean('hasPlanification')->default(0);
            $table->string('status');

            $table->timestamps(2);

            $table->foreign('enterprise_id')->references('id')->on('enterprises')/*->onDelete('cascade')*/;
            $table->foreign('department_id')->references('id')->on('departments')/*->onDelete('cascade')*/;
            $table->foreign('province_id')->references('id')->on('provinces')/*->onDelete('cascade')*/;
            $table->foreign('district_id')->references('id')->on('districts')/*->onDelete('cascade')*/;
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('constructions');
    }
}
