<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateObservationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('observations', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->unsignedBigInteger('protocol_id');
            $table->unsignedBigInteger('observation_type_id');
            $table->longText('description');
            $table->string('short_description');

            $table->timestamps(2);

            $table->foreign('protocol_id')->references('id')->on('protocols')/* ->onDelete('cascade') */;
            $table->foreign('observation_type_id')->references('id')->on('observation_types')/* ->onDelete('cascade') */;
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('observations');
    }
}
