<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePlanificationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('planifications', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->unsignedBigInteger('construction_id');
            $table->unsignedBigInteger('protocol_id');
            $table->longText('description')->nullable();
            $table->timestamp('start_at')->nullable();
            $table->timestamp('end_at')->nullable();
            $table->string('type')->default('Interno');
            $table->string('status');

            $table->timestamps(2);

            $table->foreign('construction_id')->references('id')->on('constructions')/* ->onDelete('cascade') */;
            $table->foreign('protocol_id')->references('id')->on('protocols')/* ->onDelete('cascade') */;
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('planifications');
    }
}
