<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePeopleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('people', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->unsignedBigInteger('enterprise_id');
            $table->unsignedBigInteger('user_id')->nullable();
            $table->string('first_name');
            $table->string('last_name');
            $table->string('email');
            $table->string('phone');
            $table->string('address')->nullable();
            $table->string('rut')->nullable()->unique();
            $table->date('birthdate')->nullable();
            $table->string('status');
            $table->string('type');
            $table->longText('photo')->nullable();

            $table->timestamps(2);

            $table->foreign('user_id')->references('id')->on('users')/* ->onDelete('cascade') */;
            $table->foreign('enterprise_id')->references('id')->on('enterprises')/* ->onDelete('cascade') */;
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('people');
    }
}
