<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConstructionPeopleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('construction_people', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->unsignedBigInteger('construction_id');
            $table->unsignedBigInteger('people_id');
            $table->unsignedBigInteger('people_type_id')->nullable();
            $table->boolean('leader')->default(0);

            $table->timestamps(2);

            $table->foreign('construction_id')->references('id')->on('constructions')/* ->onDelete('cascade') */;
            $table->foreign('people_id')->references('id')->on('people')/* ->onDelete('cascade') */;
            $table->foreign('people_type_id')->references('id')->on('people_types')/* ->onDelete('cascade') */;
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('construction_people');
    }
}
