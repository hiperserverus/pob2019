<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePeopleRolesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('people_roles', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->unsignedBigInteger('people_type_id');
            $table->unsignedBigInteger('people_id');

            $table->timestamps(2);

            $table->foreign('people_type_id')->references('id')->on('people_types')/* ->onDelete('cascade') */;
            $table->foreign('people_id')->references('id')->on('people')/* ->onDelete('cascade') */;
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('people_roles');
    }
}
