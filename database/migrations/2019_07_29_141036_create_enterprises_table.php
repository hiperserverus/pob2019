<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEnterprisesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('enterprises', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->unsignedBigInteger('department_id')->nullable();
            $table->unsignedBigInteger('province_id')->nullable();
            $table->unsignedBigInteger('district_id')->nullable();
            $table->longText('name');
            $table->string('auxiliaryCode')->nullable();
            $table->string('auxiliaryName')->nullable();
            $table->string('origin')->nullable();
            $table->string('auxiliarySorter')->nullable();
            $table->string('documentType')->nullable();
            $table->string('ruc')->nullable();
            $table->longText('address')->nullable();
            $table->string('ubigeo')->nullable();
            $table->longText('location')->nullable();
            $table->string('contactName')->nullable();
            $table->string('phone')->nullable();
            $table->string('fax')->nullable();
            $table->string('email')->nullable();
            $table->string('status')->nullable();

            $table->timestamps(2);
            $table->foreign('department_id')->references('id')->on('departments')/* ->onDelete('cascade') */;
            $table->foreign('province_id')->references('id')->on('provinces')/* ->onDelete('cascade') */;
            $table->foreign('district_id')->references('id')->on('districts')/* ->onDelete('cascade') */;
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('enterprises');
    }
}
