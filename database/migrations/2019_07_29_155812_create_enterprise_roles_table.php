<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEnterpriseRolesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('enterprise_roles', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->unsignedBigInteger('people_type_id');
            $table->unsignedBigInteger('enterprise_id');

            $table->timestamps(2);

            $table->foreign('people_type_id')->references('id')->on('people_types')/* ->onDelete('cascade') */;
            $table->foreign('enterprise_id')->references('id')->on('enterprises')/* ->onDelete('cascade') */;
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('enterprise_roles');
    }
}
