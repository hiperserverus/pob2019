<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateParticipantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('participants', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->unsignedBigInteger('planification_id');
            $table->unsignedBigInteger('people_id');
            $table->unsignedBigInteger('people_type_id')->nullable();
            $table->boolean('leader')->default(0);

            $table->timestamps(2);

            $table->foreign('planification_id')->references('id')->on('planifications')/* ->onDelete('cascade') */;
            $table->foreign('people_id')->references('id')->on('people')/* ->onDelete('cascade') */;
            $table->foreign('people_type_id')->references('id')->on('people_types')/* ->onDelete('cascade') */;
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('participants');
    }
}
