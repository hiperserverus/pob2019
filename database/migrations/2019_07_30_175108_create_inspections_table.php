<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInspectionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('inspections', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->unsignedBigInteger('planification_id');
            $table->longText('latitude');
            $table->longText('longitude');
            $table->string('distance')->nullable();
            $table->date('time')->nullable();
            $table->integer('total')->default(0);
            $table->string('status');

            $table->timestamps(2);

            $table->foreign('planification_id')->references('id')->on('planifications')/* ->onDelete('cascade') */;
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('inspections');
    }
}
