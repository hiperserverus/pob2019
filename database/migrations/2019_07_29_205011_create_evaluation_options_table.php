<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEvaluationOptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('evaluation_options', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->unsignedBigInteger('evaluation_id');
            $table->unsignedBigInteger('option_id');
            $table->integer('percentage')->default(5);
            $table->boolean('defaultOption')->default(0);

            $table->timestamps(2);

            $table->foreign('evaluation_id')->references('id')->on('evaluations')/* ->onDelete('cascade') */;
            $table->foreign('option_id')->references('id')->on('options')/* ->onDelete('cascade') */;
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('evaluation_options');
    }
}
