<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReportParticipantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('report_participants', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->unsignedBigInteger('inspection_id')->nullable();
            $table->unsignedBigInteger('people_id')->nullable();
            $table->unsignedBigInteger('people_type_id')->nullable();
            $table->boolean('leader')->default(0);

            $table->timestamps(2);

            $table->foreign('inspection_id')->references('id')->on('inspections')/* ->onDelete('cascade') */;
            $table->foreign('people_id')->references('id')->on('people')/* ->onDelete('cascade') */;
            $table->foreign('people_type_id')->references('id')->on('people_types')/* ->onDelete('cascade') */;
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('report_participants');
    }
}
