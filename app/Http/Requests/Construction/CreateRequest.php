<?php

namespace App\Http\Requests\Construction;

use Illuminate\Foundation\Http\FormRequest;

class CreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'description' => 'required',
            'address' => 'required',
            'latitude' => 'required',
            'longitude' => 'required',
            'status' => 'required'
        ];
    }

    public function message()
    {
        return [
            'description.required' => 'La descripción de la obra es requerida.',
            'address.required' => 'La dirección de la obra es requerida.',
            'latitude.required' => 'La latitud es requerida.',
            'longitude.required' => 'La longitud es requerida.',
            'status.required' => 'El estado de la obra es requerida.',
        ];
    }
}
