<?php

namespace App\Http\Requests\People;

use Illuminate\Foundation\Http\FormRequest;

class PeopleUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name' => 'required|string',
            'last_name' => 'required|string',
            'email' => 'required|email|unique:people,email,'.$this->id,
            'phone' => 'required|string',
            'rut' => 'required|string',
            'birthdate' => 'required',
        ];
    }

    public function message()
    {
        return [
            'first_name.required' => 'El nombre es requerido',
            'first_name.string' => 'El nombre debe de ser una cadena válida.',
            'last_name.required' => 'El apellido es requerido',
            'last_name.string' => 'El apellido debe de ser una cadena válida.',
            'email.required' => 'El correo electrónico es requerido.',
            'email.email' => 'El correo electrónico debe ser válido.',
            'email.unique' => 'El correo electrónico ingresado ha sido utilizado.',
            'phone.required' => 'El número de teléfono es requerido',
            'phone.string' => 'El número de teléfono debe de ser una cadena válida',
            'rut.required' => 'El RUT es requerido',
            'rut.string' => 'El RUT debe de ser una cadena válida',
            'birthdate' => 'La fecha de nacimiento es requerida',
        ];
    }
}
