<?php

namespace App\Http\Controllers;

use App\Model\People;
use App\Http\Requests\People\PeopleStoreRequest;
use App\Http\Requests\Auth\AppRegisterRequest;
use App\Http\Requests\Auth\AppUpdateRequest;
use App\Http\Requests\People\PeopleUpdateRequest;
use Illuminate\Http\File;
use App\Mail\UserRegistered;
use Mail;
use Carbon\Carbon;
use App\User;
use Storage;
use Image;
use Validator;
use App\Model\PeopleType;
use App\Model\Enterprise;
use App\Model\Department;
use Illuminate\Http\Request;

class PeopleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $peoples = People::with('user', 'peopleTypes', 'enterprise')->get();



        return response()->json(['peoples' => $peoples]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            $user = User::create([
                'first_name' => $request->first_name,
                'last_name' => $request->last_name,
                'email' => $request->email,
                'password' => bcrypt($request->password),
                'type' => $request->userType,
                'confirmed' => true,
            ]);

            $user_id = User::orderBy('id', 'desc')->first();

            $people = People::create([
                'enterprise_id' => $request->enterprise_id,
                'user_id' => $user_id->id,
                'first_name' => $request->first_name,
                'last_name' => $request->last_name,
                'email' => $user_id->email,
                'phone' => $request->phone,
                'address' => $request->address,
                'type' => $request->type,
                'birthdate' => $request->birthdate,
                'status' => $request->status
            ]);

            $people->peopleTypes()->sync($request->peopleTypes);

            return response()->json([
                'message' => 'success',
                'id' => $people->id
            ], 200);
        }catch (\Exception $e){
            return response()->json([
                'message' => $e->getMessage(),
            ], 400);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\People  $people
     * @return \Illuminate\Http\Response
     */
    public function show(People $person)
    {
        $people = People::where('id', $person->id)->with('user', 'enterprise', 'peopleTypes')->first();
        $peopleType = $person->peopleTypes;
        $enterprise = $person->enterprise;

        return response()->json([
            'people' => $people,
            'peopleTypes' => $peopleType,
            'enterprise' => $enterprise
        ], 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model\People  $people
     * @return \Illuminate\Http\Response
     */
    public function edit(People $people)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\People  $people
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try{
            $people = People::findOrFail($id);

            $people->update($request->all());

            $user = User::findOrFail($people->user_id);
            $user->update([
                'type' => $request->userType,
            ]);
            $people->peopleTypes()->sync($request->people_types);

            return response()->json([
                'message' => 'success',
                'id' => $people->id
            ], 200);

        }catch (\Exception $e){
            return response()->json([
                'message' => $e->getMessage(),
            ], 400);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\People  $people
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $people = People::findOrFail($id);
        $user = User::findOrFail($people->user_id);

        $people->delete();
        $user->delete();

        return response()->json(['message' => 'success'], 200);
    }


    public function allData($id)
    {
        $peoples = People::where('enterprise_id', '!=', 1)->get();

        $user = User::where('id', $id)->with('people', 'people.peopleTypes', 'people.constructions', 'people.constructions.department', 'people.constructions.province', 'people.constructions.district', 'people.planifications', 'people.constructions.planifications', 'people.planifications.peoples', 'people.planifications.peoples.peopleTypes', 'people.planifications.protocol', 'people.planifications.protocol.observations', 'people.planifications.protocol.observations.observation_type', 'people.planifications.protocol.observations.evaluations', 'people.planifications.protocol.observations.evaluations.options')->first();

        $peopleTypes = PeopleType::get();

        $departments = Department::get();

        return response()->json([
            'user' => $user,
            'peopleTypes' => $peopleTypes,
            'peoples' => $peoples,
            'departments' => $departments,
        ]);
    }

    public function constructionPerUser($id)
    {
        $people = People::where('id', $id)->with('constructions', 'constructions.department', 'constructions.province', 'constructions.district', 'constructions.enterprise')->get();

        return response()->json([
            'people' => $people
        ]);
    }

    public function planificationPerUser($id)
    {
        $people = People::where('id', $id)->with('planifications', 'planifications.construction', 'planifications.construction.enterprise', 'planifications.inspection', 'planifications.peoples', 'planifications.peoples.peopleTypes', 'planifications.protocol', 'planifications.protocol.observations', 'planifications.protocol.observations.observation_type', 'planifications.protocol.observations.evaluations', 'planifications.protocol.observations.evaluations.options')->get();

        return response()->json([
            'people' => $people
        ]);
    }

    public function inspectionPerUser($id)
    {
        $people = People::where('id', $id)->with('inspections', 'inspections.peoples', 'inspections.peoples.peopleTypes', 'inspections.planification', 'inspections.reports', 'inspections.reports.observation', 'inspections.reports.observation.observation_type', 'inspections.reports.evaluation', 'inspections.reports.option')->first();

        return response()->json([
            'people' => $people
        ]);
    }

    public function appRegister(Request $request)
    {

        $peoples = People::get();

        $validate = false;

        foreach ($peoples as $person) {
            if ($person->email === $request->email) {
                $validate = true;
            }
        }

        try{

            if ($validate === false) {

                $user = User::create([
                    'first_name' => $request->first_name,
                    'last_name' => $request->last_name,
                    'email' => $request->email,
                    'password' => bcrypt($request->password),
                    'type' => 'Móvil'
                ]);
                $user_id = User::orderBy('id', 'desc')->first();

                $people = People::create([
                    'enterprise_id' => $request->enterprise_id,
                    'user_id' => $user_id->id,
                    'first_name' => $request->first_name,
                    'last_name' => $request->last_name,
                    'email' => $user_id->email,
                    'phone' => $request->phone,
                    'address' => $request->address,
                    'type' => $request->type,
                    'rut' => $request->rut,
                    'birthdate' => $request->birthdate,
                    'status' => $request->status
                ]);

                $people->peopleTypes()->sync($request->peopleTypes);
                $data = $people->first_name .' '. $people->last_name;

                Mail::to($user->email)->send(new UserRegistered($data));

                return response()->json([
                    'message' => 'success',
                    'id' => $user->id
                ], 200);

            }else {
                return response()->json([
                    'message' => 'Correo en uso',
                ], 401);
            }

        }catch (\Exception $e){
            return response()->json([
                'message' => $e->getMessage(),
            ], 400);
        }
    }

    public function changeStatus($id)
    {
        try{
            $people = People::findOrFail($id);
            $people->update(['status' => 'Confirmado']);
            $user = User::findOrFail($id);
            $user->update(['confirmed' => true]);
            $data = $people->first_name .' '. $people->last_name;

            Mail::to($people->email)->send(new UserRegistered($data));

            return response()->json(['message' => 'success'], 200);

        }catch (\Exception $e){
            return response()->json([
                'message' => $e->getMessage(),
            ], 408);
        }
    }

    public function updateProfile(Request $request, $id)
    {
        $people = People::findOrFail($id);

        $peoples = People::where('id', '!=', $id)->get();

        $validate = false;

        foreach ($peoples as $person) {
            if ($person->email === $request->email) {
                $validate = true;
            }
        }

        try{

            if($validate === false) {
                $people->update([
                    'first_name' => $request->first_name,
                    'last_name' => $request->last_name,
                    'enterprise_id' => $request->enterprise_id,
                    'email' => $request->email,
                    'phone' => $request->phone,
                    'address' => $request->address,
                    'rut' => $request->rut,
                    'birthdate' => $request->birthdate,
                ]);

                $people->peopleTypes()->sync($request->peopleTypes);

                $user = User::findOrFail($people->user_id);

                if ($request->password != null)
                {
                    $user->update([
                        'email' => $people->email,
                        'password' => bcrypt($request->password)
                    ]);
                }else{
                    $user->update([
                        'email' => $people->email
                    ]);
                }

                return response()->json([
                    'message' => 'success',
                    'id' => $people->id
                ], 200);

            }else {
                return response()->json([
                    'message' => 'Correo en uso',
                ], 401);
            }

        }catch (\Exception $e){
            return response()->json([
                'message' => $e->getMessage(),
            ], 400);
        }
    }

    public function updateProfilePhoto(Request $request, $id)
    {
        $people = People::findOrFail($id);

        try {

            if ($request->photo)
            {
                $photo = 'photo-' . $people->last_name . '-' . $people->first_name . '-' . $people->id . '.png';
                Storage::putFileAs('/public/people/' . $people->id . '-photo', new File($request->file('photo')), $photo);

                $people->update([
                    'photo' => $photo
                ]);

                return response()->json([
                    'message' => 'success',
                    'id' => $people->id
                ], 200);
            } else {

                return response()->json([
                    'id' => $people->id
                ], 200);

            }

        }catch (\Exception $e) {
            return response()->json([
                'message' => $e->getMessage(),
            ], 400);
        }
    }
}
