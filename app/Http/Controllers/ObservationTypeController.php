<?php

namespace App\Http\Controllers;

use App\Model\ObservationType;
use Illuminate\Http\Request;

class ObservationTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $observationTypes = ObservationType::with('observations')->get();

        return response()->json(['observationTypes' => $observationTypes]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            ObservationType::create($request->all());

            return response()->json(['message' => 'success'], 200);

        }catch(\Exception $e){
            return response()->json([
                'message' => $e->getMessage(),
            ], 400);
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\ObservationType  $observationType
     * @return \Illuminate\Http\Response
     */
    public function show(ObservationType $observationType)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model\ObservationType  $observationType
     * @return \Illuminate\Http\Response
     */
    public function edit(ObservationType $observationType)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\ObservationType  $observationType
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $observationType = ObservationType::findOrFail($id);
            $observationType->update($request->all());

            return response()->json(['message' => 'success'], 200);

        }catch(\Exception $e){
            return response()->json([
                'message' => $e->getMessage(),
            ], 400);
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\ObservationType  $observationType
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $observationType = ObservationType::findOrFail($id);
        $observationType->delete();

        return response()->json(['message' => 'success'], 200);
    }
}
