<?php

namespace App\Http\Controllers;

use App\Model\Construction;
use App\Model\People;
use App\Model\PeopleType;
use App\Model\Planification;
use Illuminate\Http\Request;
use App\Mail\UpdatedConstructionStatus;
use Mail;
use Carbon\Carbon;

class ConstructionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $constructions = Construction::with('enterprise', 'peoples', 'peoples.peopleTypes')->get();

        return response()->json(['constructions' => $constructions]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            $construction = Construction::create($request->all());

            for ($i=0; $i < count($request->selected); $i++) {
                $construction->peoples()->attach($request->selected[$i]['peoples']['id'], [
                    'people_type_id' => $request->selected[$i]['peopleTypes']['id'],
                    'leader' => $request->leader === $request->selected[$i]['peoples']['id'] ? 1 : 0,
                ]);
            }

            return response()->json([
                'message' => 'success',
                'id' => $construction->id,
            ], 200);
        }catch (\Exception $e){
            return response()->json([
                'message' => $e->getMessage(),
            ], 400);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\Construction  $construction
     * @return \Illuminate\Http\Response
     */
    public function show(Construction $construction)
    {
        $construction = Construction::where('id', $construction->id)->with('peoples', 'department', 'province', 'district', 'enterprise', 'peoples.peopleTypes')->first();
        $peoples = $construction->peoples()->get();
        $selected = [];

        foreach ($peoples as $people) {
            $peopleType = PeopleType::where('id', $people->pivot->people_type_id)->first();
            array_push($selected, [
                'peoples' => $people,
                'peopleTypes' => $peopleType,
                'index' => $people->pivot->leader === 1 ? 1 : 0,
            ]);
        }

        return response()->json([
            'construction' => $construction,
            /* 'peoples' => $peoples, */
            'selected' => $selected
        ], 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model\Construction  $construction
     * @return \Illuminate\Http\Response
     */
    public function edit(Construction $construction)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\Construction  $construction
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Construction $construction)
    {
        try{
            $construction->update($request->all());

            if($request->selected){
                $construction->peoples()->detach();
                for ($i=0; $i < count($request->selected); $i++) {
                    $construction->peoples()->attach($request->selected[$i]['peoples']['id'], [
                        'people_type_id' => $request->selected[$i]['peopleTypes']['id'],
                        'leader' => $request->leader === $request->selected[$i]['peoples']['id'] ? 1 : 0,
                    ]);
                }
            }

            return response()->json([
                'message' => 'success',
                'id' => $construction->id
            ], 200);

        }catch (\Exception $e){
            return response()->json([
                'message' => $e->getMessage(),
            ], 400);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\Construction  $construction
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $construction = Construction::findOrFail($id);
        $construction->delete();

        return response()->json(['message' => 'success'], 200);
    }

    public function changeStatus($id)
    {
        try{
            $construction = Construction::findOrFail($id);
            $people = People::findOrFail($construction->peoples->pluck('id'));
            $construction->update(['status' => 'Confirmada']);

            $data = $people[0]->first_name .' '. $people[0]->last_name;

            Mail::to($people[0]->email)->send(new UpdatedConstructionStatus($data));

            return response()->json([
                'message' => 'success',
                'people_email' => $people[0]->email,
                'data' => $data
            ], 200);
        }catch (\Exception $e){
            return response()->json([
                'message' => $e->getMessage(),
            ], 408);
        }
    }
}
