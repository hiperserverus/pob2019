<?php

namespace App\Http\Controllers;

use App\Model\Evaluation;
use Illuminate\Http\Request;

class EvaluationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $evaluations = Evaluation::with('observation')->get();

        return response()->json(['evaluations' => $evaluations]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            $evaluation = Evaluation::create($request->all());

            foreach ($request->options as $option)
            {
                foreach ($request->percentagePerOption as $key => $percent)
                {
                    if ($option === $key && $option)
                    {
                        $evaluation->options()->attach($option, [
                            'percentage' => $percent,
                            'defaultOption' => $option === $request->defaultOption ? 1 : 0
                        ]);
                    }
                }
            }

            return response()->json([
                'message' => 'success',
                'id' => $evaluation->id
            ], 200);

        }catch(\Exception $e){
            return response()->json([
                'message' => $e->getMessage(),
            ], 400);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\Evaluation  $evaluation
     * @return \Illuminate\Http\Response
     */
    public function show(Evaluation $evaluation)
    {
        $evaluation = Evaluation::where('id', $evaluation->id)->with('observation', 'options')->first();

        $percentagePerOption = [];
        $options = [];
        $defaultOption = 0;

        foreach ($evaluation->options as $eval) {
            array_push($percentagePerOption, [$eval->pivot->option_id => $eval->pivot->percentage]);
            array_push($options, $eval->pivot->option_id);
            if ($eval->pivot->defaultOption === 1) {
                $defaultOption = $eval->pivot->option_id;
            }
        }

        return response()->json([
            'evaluation' => $evaluation,
            'options' => $options,
            'percentagePerOption' => $percentagePerOption,
            'defaultOption' => $defaultOption
        ], 200);
    }

     /**
     *  Show the form for editing the specified resource.
     *
     * @param  \App\Model\Evaluation  $evaluation
     * @return \Illuminate\Http\Response
     */
    public function edit(Evaluation $evaluation)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\Evaluation  $evaluation
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try{
            $evaluation = Evaluation::findOrFail($id);
            $evaluation->update($request->all());

            if ($request->options)
            {
                $evaluation->options()->detach();
                foreach ($request->options as $option)
                {
                    foreach ($request->percentagePerOption as $key => $percent)
                    {
                        if ($option === $key && $option)
                        {
                            $evaluation->options()->attach($option,
                            [
                                'percentage' => $percent,
                                'defaultOption' => $option === $request->defaultOption ? 1 : 0
                            ]);
                        }
                    }
                }
            }

            return response()->json([
                'message' => 'success',
                'id' => $evaluation->id
            ], 200);

        }catch(\Exception $e){
            return response()->json([
                'message' => $e->getMessage(),
            ], 400);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\Evaluation  $evaluation
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $option = Evaluation::findOrFail($id);
        $option->delete();

        return response()->json(['message' => 'success'], 200);
    }
}
