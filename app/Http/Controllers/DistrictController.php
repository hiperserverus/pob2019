<?php

namespace App\Http\Controllers;

use App\Model\District;
use Illuminate\Http\Request;

class DistrictController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $districts = District::with('province', 'province.department')->get();

        return response()->json([
            'districts' => $districts
        ], 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $district = District::create($request->all());

            return response()->json([
                'message' => 'success',
                'id' => $district->id,
            ], 200);

        }catch (\Exception $e){

            return response()->json([
                'message' => $e->getMessage(),
            ], 400);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\District  $district
     * @return \Illuminate\Http\Response
     */
    public function show(District $district)
    {
        try {

            $district = District::where('id', $district->id)->with('department', 'districts')->first();

            return response()->json([
                'district' => $district,
            ], 200);

        }catch (\Exception $e){

            return response()->json([
                'message' => $e->getMessage(),
            ], 404);

        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model\District  $district
     * @return \Illuminate\Http\Response
     */
    public function edit(District $district)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\District  $district
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, District $district)
    {
        try{

            $district->update($request->all());

            return response()->json([
                'message' => 'Success',
                'id' => $district->id
            ], 200);

        }catch(\Exception $e) {

            return response()->json([
                'message' => $e->getMessage(),
            ], 400);

        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\District  $district
     * @return \Illuminate\Http\Response
     */
    public function destroy(District $district)
    {
        $district->delete();

        return response()->json([
            'message' => 'Successfully'
        ], 200);
    }
}
