<?php

namespace App\Http\Controllers;

use Mail;
use Storage;
use PDF;
use Carbon\Carbon;
use Illuminate\Http\File;
use Illuminate\Http\Request;
use App\Mail\ReportStored;
use App\Model\Report;
use App\Model\Inspection;
use App\Model\Planification;
use App\Model\Construction;
use App\Model\ObservationType;
use App\Model\Observation;
use App\Model\Image;

class ReportController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $reports = Report::get();

        return response()->json(['reports' => $reports]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $idReport = [];

        try{

            if ($request->reports != null) {

                foreach ($request->reports as $report) {
                    $inspecReport = Report::create([
                        'inspection_id' => $report['inspection_id'],
                        'observation_id' => $report['observation_id'],
                        'evaluation_id' => $report['evaluation_id'],
                        'option_id' => $report['option_id'],
                        'comment' => $report['comment'],
                        'urlImage' => $report['urlImage'],
                        'urlVideo' => $report['urlVideo'],
                        'evaluation_percentage' => $report['evaluation_percentage'],
                        'evaluation_option_percentage' => $report['evaluation_option_percentage']
                    ]);

                    if ($report['urlImage'] || $report['urlVideo']) {
                        array_push($idReport, ['report_id' => $inspecReport->id, 'evaluation_id' => $report['evaluation_id']]);
                    }
                }
            }
            $inspection = Inspection::findOrFail($request->inspection['id']);

            for ($i=0; $i < count($request->selected); $i++) {
                $inspection->peoples()->attach($request->selected[$i]['peoples']['id'], [
                    'people_type_id' => $request->selected[$i]['peopleTypes']['id'],
                    'leader' => $request->leader === $request->selected[$i]['peoples']['id'] ? 1 : 0,
                ]);
            }

            if (!$idReport) {

                $inspection->status = 'Terminado';
                $inspection->total = $request->inspection['total'];
                $inspection->save();

                $planification = Planification::findOrFail($inspection->planification_id);
                $planification->status = 'Terminado';
                $planification->save();

                $construction = Construction::findOrFail($planification->construction_id);
                $construction->hasPlanification = false;
                $construction->save();

                /* PDF */

                $inspection = Inspection::where('id', $request->inspection['id'])->with('peoples', 'peoples.enterprise', 'planification', 'planification.construction', 'planification.construction.district', 'planification.construction.enterprise', 'reports', 'reports.observation', 'reports.observation.observation_type', 'reports.evaluation', 'reports.option')->first();

                $enterprise = $inspection->planification->construction->enterprise->name;

                $day = Carbon::now()->format('d');
                $month = Carbon::now()->locale('es_ES')->monthName;
                $month = ucfirst($month);
                $year = Carbon::now()->year;

                $today = 'Lima ' . $day . ' de ' . $month . ' del ' . $year;

                $installer = null;

                foreach ($inspection->peoples as $people) {
                    if ($people->pivot->leader === 1) {
                        $installer = $people->first_name . ' ' . $people->last_name;
                    }
                }

                $observationTypes = ObservationType::get();
                $observationsG = Observation::where('observation_type_id', 1)->get();
                $observationsS = Observation::where('observation_type_id', 2)->get();
                $auxObservationType = [];
                $observationTypeAvailable = [];
                $auxObservationG = [];
                $observationGAvailable = [];
                $auxObservationS = [];
                $observationSAvailable = [];

                foreach ($inspection->reports as $report) {
                    foreach ($observationTypes as $observationType) {
                        if ($report->observation->observation_type_id === $observationType->id) {
                            array_push($auxObservationType, $observationType->id);
                        }
                    }
                    foreach ($observationsG as $observationG) {
                        if ($report->observation_id === $observationG->id) {
                            array_push($auxObservationG, $observationG->id);
                        }
                    }
                    foreach ($observationsS as $observationS) {
                        if ($report->observation_id === $observationS->id) {
                            array_push($auxObservationS, $observationS->id);
                        }
                    }
                }
                $auxObservationType = array_unique($auxObservationType);
                $auxObservationG = array_unique($auxObservationG);
                $auxObservationS = array_unique($auxObservationS);

                foreach ($auxObservationType as $a) {
                    array_push($observationTypeAvailable, $a);
                }

                foreach ($auxObservationG as $a) {
                    array_push($observationGAvailable, $a);
                }
                foreach ($auxObservationS as $a) {
                    array_push($observationSAvailable, $a);
                }


                $pdf = PDF::loadView('pdf.report', compact('installer', 'today', 'day', 'month', 'enterprise', 'inspection', 'observationTypeAvailable', 'observationGAvailable', 'observationSAvailable'));
                $pdfRoute = 'public/reports/pdf/informe_supervisión_' . $inspection->id . '.pdf';
                $pdfName = 'informe_supervisión_' . $inspection->id . '.pdf';
                Storage::put($pdfRoute, $pdf->output());

                /* Fin PDF */

                /* Send MAIL */
                $emails = [];

                foreach ($inspection->peoples as $people) {
                    array_push($emails, $people->email);
                }

                try {
                    Mail::send('emails.report', [], function($message)use($emails, $pdf) {
                        $message->to($emails)
                        ->subject('Informe de supervisión de obras')
                        ->attachData($pdf->output(), 'informe-supervision-obras.pdf');
                        });
                }catch(JWTException $exception){
                    $this->serverstatuscode = "0";
                    $this->serverstatusdes = $exception->getMessage();
                }

                /* End Send MAIL */
            } else {

                $inspection->total = $request->inspection['total'];
                $inspection->save();

            }

            return response()->json([
                'message' => 'Informe enviado.',
                'inspection_id' => $inspection->id,
                'reports_id' => $idReport,
            ], 200);

        }catch(\Exception $e){
            return response()->json([
                'message' => $e->getMessage(),
            ], 400);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\Report  $report
     * @return \Illuminate\Http\Response
     */
    public function show(Report $report)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model\Report  $report
     * @return \Illuminate\Http\Response
     */
    public function edit(Report $report)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\Report  $report
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $report = Report::findOrFail($id);

        try{
            $video = null;
            $image = null;

            if ($request->hasFile('urlImage')) {
                $imageExtension = $request->urlImage->getClientOriginalExtension();
                $image = 'image-' . $report->id . '' . $report->observation_id . '' . $report->evaluation_id . '' . $report->option_id . '-' . str_random(5) . '.' . $imageExtension;

                Storage::putFileAs('/public/report/' . $report->id . '-report/image', new File($request->file('urlImage')), $image);
            }

            if ($request->hasFile('urlVideo')) {
                $video = 'video-' . $report->id . '' . $report->observation_id . '' . $report->evaluation_id . '' . $report->option_id . '-' . str_random(5) . '.mp4';
                Storage::putFileAs('/public/report/' . $report->id . '-report/video', new File($request->file('urlVideo')), $video);
            }

            if ($image === null) {
                return response()->json([
                    'message' => 'Error al subir la imagen.'
                ],400);
            } else {
                $image = Image::create([
                    'path' => $image,
                ]);

                $image->reports()->sync($report->id);

                return response()->json([
                    'message' => 'success',
                    'id' => $report->id
                ], 200);
            }

        }catch(\Exception $e){
            return response()->json([
                'message' => $e->getMessage(),
            ], 400);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\Report  $report
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $report = Report::findOrFail($id);
        $report->delete();

        return response()->json(['message' => 'success'], 200);
    }

    public function finishPlanification(Request $request, $id)
    {

        try {
            $inspection = Inspection::where('id', $id)->with('peoples', 'peoples.enterprise', 'planification', 'planification.construction', 'planification.construction.district', 'planification.construction.enterprise', 'reports', 'reports.observation', 'reports.observation.observation_type', 'reports.evaluation', 'reports.option')->first();
            $inspection->status = $request->statusInspection;
            $inspection->save();
            $planification = Planification::findOrFail($inspection->planification_id);
            $planification->status = $request->statusPlanification;
            $planification->save();
            $construction = Construction::findOrFail($planification->construction_id);
            $construction->hasPlanification = $request->hasPlanification;
            $construction->save();

            /* PDF */

            $enterprise = $inspection->planification->construction->enterprise->name;

            $day = Carbon::now()->format('d');
            $month = Carbon::now()->locale('es_ES')->monthName;
            $month = ucfirst($month);
            $year = Carbon::now()->year;

            $today = 'Lima ' . $day . ' de ' . $month . ' del ' . $year;

            $installer = null;

            foreach ($inspection->peoples as $people) {
                if ($people->pivot->leader === 1) {
                    $installer = $people->first_name . ' ' . $people->last_name;
                }
            }

            $observationTypes = ObservationType::get();
            $observationsG = Observation::where('observation_type_id', 1)->get();
            $observationsS = Observation::where('observation_type_id', 2)->get();
            $auxObservationType = [];
            $observationTypeAvailable = [];
            $auxObservationG = [];
            $observationGAvailable = [];
            $auxObservationS = [];
            $observationSAvailable = [];

            foreach ($inspection->reports as $report) {
                foreach ($observationTypes as $observationType) {
                    if ($report->observation->observation_type_id === $observationType->id) {
                        array_push($auxObservationType, $observationType->id);
                    }
                }
                foreach ($observationsG as $observationG) {
                    if ($report->observation_id === $observationG->id) {
                        array_push($auxObservationG, $observationG->id);
                    }
                }
                foreach ($observationsS as $observationS) {
                    if ($report->observation_id === $observationS->id) {
                        array_push($auxObservationS, $observationS->id);
                    }
                }
            }
            $auxObservationType = array_unique($auxObservationType);
            $auxObservationG = array_unique($auxObservationG);
            $auxObservationS = array_unique($auxObservationS);

            foreach ($auxObservationType as $a) {
                array_push($observationTypeAvailable, $a);
            }

            foreach ($auxObservationG as $a) {
                array_push($observationGAvailable, $a);
            }
            foreach ($auxObservationS as $a) {
                array_push($observationSAvailable, $a);
            }


            $pdf = PDF::loadView('pdf.report', compact('installer', 'today', 'day', 'month', 'enterprise', 'inspection', 'observationTypeAvailable', 'observationGAvailable', 'observationSAvailable'));
            $pdfRoute = 'public/reports/pdf/informe_supervisión_' . $inspection->id . '.pdf';
            $pdfName = 'informe_supervisión_' . $inspection->id . '.pdf';
            Storage::put($pdfRoute, $pdf->output());

            /* Fin PDF */

            /* Send MAIL */
            $emails = [];

            foreach ($inspection->peoples as $people) {
                array_push($emails, $people->email);
            }

            try {
                Mail::send('emails.report', [], function($message)use($emails, $pdf) {
                    $message->to($emails)
                    ->subject('Informe de supervisión de obras')
                    ->attachData($pdf->output(), 'informe-supervision-obras.pdf');
                    });
            }catch(JWTException $exception){
                $this->serverstatuscode = "0";
                $this->serverstatusdes = $exception->getMessage();
            }

            /* End Send MAIL */

            return response()->json([
                'message' => 'status changed.'
            ], 200);

        }catch(\Exception $e){
            return response()->json([
                'message' => $e->getMessage(),
            ], 400);
        }
    }
}
