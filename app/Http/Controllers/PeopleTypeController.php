<?php

namespace App\Http\Controllers;

use App\Model\PeopleType;
use Illuminate\Http\Request;
use App\Http\Requests\People\PeopleTypeRequest;

class PeopleTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $peopleTypes = PeopleType::with('peoples')->get();

        return response()->json(['peopletypes' => $peopleTypes]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            $peopleType = PeopleType::create($request->all());

            return response()->json(['message' => 'success'], 200);

        }catch (\Exception $e){
            return response()->json([
                'message' => $e->getMessage(),
            ], 400);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\PeopleType  $peopleType
     * @return \Illuminate\Http\Response
     */
    public function show(PeopleType $peopleType)
    {
        $peopleType = PeopleType::where('id', $peopleType->id)->with('peoples', 'peoples.enterprise')->first();

        return response()->json([
            'peopleType' => $peopleType,
        ], 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model\PeopleType  $peopleType
     * @return \Illuminate\Http\Response
     */
    public function edit(PeopleType $peopleType)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\PeopleType  $peopleType
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PeopleType $people_type)
    {
        try{
            $people_type->update($request->all());

            return response()->json(['message' => 'success'], 200);

        }catch (\Exception $e){
            return response()->json([
                'message' => $e->getMessage(),
            ], 400);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\PeopleType  $peopleType
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $peopleType = PeopleType::findOrFail($id);

        $peopleType->delete();

        return response()->json(['message' => 'success'], 200);
    }
}
