<?php

namespace App\Http\Controllers;

use App\Model\Enterprise;
use Illuminate\Http\Request;

class EnterpriseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $enterprises = Enterprise::with('persons')->get();

        return response()->json(['enterprises' => $enterprises]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function indexAPP()
    {
        $enterprises = Enterprise::where('id', '!=', 1)->with('persons')->get();

        return response()->json(['enterprises' => $enterprises]);
    }

    public function constructionEnterprise()
    {
        $enterprises = Enterprise::where('id', '>', 2)->with('persons')->get();

        return response()->json(['enterprises' => $enterprises]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            $enterprise = Enterprise::create($request->all());

            $enterprise->peopleTypes()->sync($request->peopleTypes);

            return response()->json(['message' => 'success'], 200);

        }catch (\Exception $e){
            return response()->json([
                'message' => $e->getMessage(),
            ], 400);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\Enterprise  $enterprise
     * @return \Illuminate\Http\Response
     */
    public function show(Enterprise $enterprise)
    {
        $enterprise = Enterprise::where('id', $enterprise->id)->with('persons', 'peopleTypes', 'persons.peopleTypes', 'constructions', 'constructions.peoples', 'constructions.peoples.peopleTypes')->first();

        return response()->json([
            'enterprise' => $enterprise,
        ], 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model\Enterprise  $enterprise
     * @return \Illuminate\Http\Response
     */
    public function edit(Enterprise $enterprise)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\Enterprise  $enterprise
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try{
            $enterprise = Enterprise::findOrFail($id);

            $enterprise->update($request->all());
            $enterprise->peopleTypes()->sync($request->people_types);

            return response()->json(['message' => 'success'], 200);

        }catch (\Exception $e){
            return response()->json([
                'message' => $e->getMessage(),
            ], 400);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\Enterprise  $enterprise
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $enterprise = Enterprise::findOrFail($id);

        $enterprise->delete();

        return response()->json(['message' => 'success'], 200);
    }
}
