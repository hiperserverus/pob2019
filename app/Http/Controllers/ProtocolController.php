<?php

namespace App\Http\Controllers;

use App\Model\Protocol;
use Illuminate\Http\Request;

class ProtocolController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $protocols = Protocol::with('observations', 'observations.evaluations')->get();

        return response()->json(['protocols' => $protocols]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            Protocol::create($request->all());

            return response()->json(['message' => 'success'], 200);

        }catch(\Exception $e){
            return response()->json([
                'message' => $e->getMessage(),
            ], 400);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\Protocol  $protocol
     * @return \Illuminate\Http\Response
     */
    public function show(Protocol $protocol)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model\Protocol  $protocol
     * @return \Illuminate\Http\Response
     */
    public function edit(Protocol $protocol)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\Protocol  $protocol
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try{
            $protocol = Protocol::findOrFail($id);
            $protocol->update($request->all());

            return response()->json(['message' => 'success'], 200);

        }catch(\Exception $e){
            return response()->json([
                'message' => $e->getMessage(),
            ], 400);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\Protocol  $protocol
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $protocol = Protocol::where('id', $id)->with('planifications')->first();

        $count = 0;

        foreach ($protocol->planifications as $planification) {
            if ($planification->status !== 'Terminada') {
                $count++;
            }
        }

        if ($count === 0) {
            $protocol->delete();

            return response()->json(['message' => 'success'], 200);

        } else {

            return response()->json(['message' => 'Existe planificaciones en curso'], 401);
        }
    }

    public function copy($id)
    {
        try{
            $protocol = Protocol::findOrFail($id);
            $newProtocol = $protocol->replicate();
            $newProtocol->name = $newProtocol->name .' "copia"';
            $newProtocol->status = 'Inactivo';
            $newProtocol->save();
            $optionsID = [];

            foreach ($protocol->observations as $observation) {
                $newObservation = $observation->replicate();
                $newObservation->description = $newObservation->description. ' "copia"';
                $newObservation->protocol_id = $newProtocol->id;
                $newObservation->save();
                foreach ($observation->evaluations as $evaluation) {
                    $newEvaluation = $evaluation->replicate();
                    $newEvaluation->observation_id = $newObservation->id;
                    $newEvaluation->description = $newEvaluation->description. ' "copia"';
                    $newEvaluation->save();
                    foreach ($evaluation->options as $option) {
                        array_push($optionsID, $option->id);
                    }
                    $newEvaluation->options()->sync($optionsID);
                }
            }

            return response()->json(['message' => 'success'],200);

        }catch(\Exception $e){
            return response()->json([
                'message' => $e->getMessage(),
            ], 408);
        }
    }
}
