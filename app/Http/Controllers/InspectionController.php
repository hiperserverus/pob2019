<?php

namespace App\Http\Controllers;

use App\Model\Inspection;
use App\Model\Report;
use App\Model\Planification;
use App\Model\ObservationType;
use App\Model\Observation;
use App\Model\Construction;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Storage;
use PDF;

class InspectionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $inspections = Inspection::with('planification', 'planification.construction')->get();

        return response()->json(['inspections' => $inspections]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $planification = Planification::findOrFail($request->planification_id);

        try{
            $inspection = Inspection::create($request->all());

            $planification->update([
                'status' => 'En proceso'
            ]);

            return response()->json([
                'message' => 'success',
                'id' => $inspection->id
            ], 200);

        }catch(\Exception $e){
            return response()->json([
                'message' => $e->getMessage(),
            ], 400);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\Inspection  $inspection
     * @return \Illuminate\Http\Response
     */
    public function show(Inspection $inspection)
    {
        $inspection = Inspection::where('id', $inspection->id)->with('planification', 'reports', 'reports.observation', 'reports.observation.observation_type', 'reports.evaluation', 'reports.option')->first();

        $reports = Report::where('inspection_id', $inspection->id)->with('observation', 'observation.observation_type', 'evaluation', 'option')->get();

        return response()->json([
            'inspection' => $inspection,
            'reports' => $reports,
        ], 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model\Inspection  $inspection
     * @return \Illuminate\Http\Response
     */
    public function edit(Inspection $inspection)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\Inspection  $inspection
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Inspection $inspection)
    {
        try{
            Inspection::whereId($inspection->id)->update([
                'status' => $request->status
            ]);

            return response()->json([
                'message' => 'success',
                'id' => $inspection->id
            ], 200);

        }catch(\Exception $e){
            return response()->json([
                'message' => $e->getMessage(),
            ], 400);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\Inspection  $inspection
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $inspection = Inspection::findOrFail($id);
        $inspection->delete();

        return response()->json(['message' => 'success'], 200);
    }

    public function pdf($id)
    {
        $inspection = Inspection::where('id', $id)->with('peoples', 'peoples.enterprise', 'planification', 'planification.construction', 'planification.construction.district', 'planification.construction.enterprise', 'reports', 'reports.observation', 'reports.observation.observation_type', 'reports.evaluation', 'reports.option')->first();

        $enterprise = $inspection->planification->construction->enterprise->name;

        $day = Carbon::now()->format('d');
        $month = Carbon::now()->locale('es_ES')->monthName;
        $month = ucfirst($month);
        $year = Carbon::now()->year;

        $today = 'Lima ' . $day . ' de ' . $month . ' del ' . $year;

        $installer = null;

        foreach ($inspection->peoples as $people) {
            if ($people->pivot->leader === 1) {
                $installer = $people->first_name . ' ' . $people->last_name;
            }
        }

        $observationTypes = ObservationType::get();
        $observationsG = Observation::where('observation_type_id', 1)->get();
        $observationsS = Observation::where('observation_type_id', 2)->get();
        $auxObservationType = [];
        $observationTypeAvailable = [];
        $auxObservationG = [];
        $observationGAvailable = [];
        $auxObservationS = [];
        $observationSAvailable = [];

        foreach ($inspection->reports as $report) {
            foreach ($observationTypes as $observationType) {
                if ($report->observation->observation_type_id === $observationType->id) {
                    array_push($auxObservationType, $observationType->id);
                }
            }
            foreach ($observationsG as $observationG) {
                if ($report->observation_id === $observationG->id) {
                    array_push($auxObservationG, $observationG->id);
                }
            }
            foreach ($observationsS as $observationS) {
                if ($report->observation_id === $observationS->id) {
                    array_push($auxObservationS, $observationS->id);
                }
            }
        }
        $auxObservationType = array_unique($auxObservationType);
        $auxObservationG = array_unique($auxObservationG);
        $auxObservationS = array_unique($auxObservationS);

        foreach ($auxObservationType as $a) {
            array_push($observationTypeAvailable, $a);
        }

        foreach ($auxObservationG as $a) {
            array_push($observationGAvailable, $a);
        }
        foreach ($auxObservationS as $a) {
            array_push($observationSAvailable, $a);
        }


        $pdf = PDF::setOptions(['isHtml5ParserEnabled' => true, 'isRemoteEnabled' => true])->loadView('pdf.report', compact('installer', 'today', 'day', 'month', 'enterprise', 'inspection', 'observationTypeAvailable', 'observationGAvailable', 'observationSAvailable'))->setPaper('A4', 'portrait');
        $pdfRoute = 'public/reports/pdf/informe_supervisión_' . $inspection->id . '.pdf';
        $pdfName = 'informe_supervisión_' . $inspection->id . '.pdf';
        Storage::put($pdfRoute, $pdf->output());

        return $pdf->download($pdfName);
    }

    /**
     * Return distance between two points
     */

    public function calculateDistance($point1_lat, $point1_long, $point2_lat, $point2_long)
    {

        $distance = rad2deg(acos((sin(deg2rad($point1_lat))*sin(deg2rad($point2_lat))) + (cos(deg2rad($point1_lat))*cos(deg2rad($point2_lat))*cos(deg2rad($point1_long-$point2_long)))));

        $convertToMeter = $distance * 111133.84;

        return round($convertToMeter, 2);
    }

    public function validateLocation(Request $request)
    {
        $planification = Planification::findOrFail($request->planification_id);
        $construction = Construction::findOrFail($planification->construction_id);

        $point1_lat = $construction->latitude;
        $point1_long = $construction->longitude;
        $point2_lat = $request->latitude;
        $point2_long = $request->longitude;

        $distance = $this->calculateDistance($point1_lat, $point1_long, $point2_lat, $point2_long);

        if($distance <= 200)
        {
            return response()->json([
                'message' => 'Success',
            ], 200);
        } else {
            return response()->json([
                'message' => 'Fuera de radio'
            ], 401);
        }
    }
}
