<?php

namespace App\Http\Controllers;

use App\Model\Planification;
use App\Model\Construction;
use App\Model\PeopleType;
use Illuminate\Http\Request;

class PlanificationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $planifications = Planification::with('construction', 'protocol')->get();

        return response()->json(['planifications' => $planifications]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            $planification = Planification::create([
                'construction_id' => $request->construction_id,
                'protocol_id' => $request->protocol_id,
                'description' => $request->description,
                'start_at' => $request->start_date . ' '. $request->start_hour,
                'end_at' => $request->end_date . ' '. $request->end_hour,
                'status' => 'Asignado'
            ]);

            $construction = Construction::find($planification->construction_id);
            $construction->hasPlanification = true;
            $construction->save();

            for ($i=0; $i < count($request->selected); $i++) {
                $planification->peoples()->attach($request->selected[$i]['peoples']['id'], [
                    'people_type_id' => $request->selected[$i]['peopleTypes']['id'],
                    'leader' => $request->leader === $request->selected[$i]['peoples']['id'] ? 1 : 0,
                ]);
            }

            return response()->json([
                'message' => 'success',
                'id' => $planification->id,
            ], 200);

        }catch(\Exception $e){
            return response()->json([
                'message' => $e->getMessage(),
            ], 400);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\Planification  $planification
     * @return \Illuminate\Http\Response
     */
    public function show(Planification $planification)
    {
        $planification = Planification::where('id', $planification->id)->with('construction', 'protocol', 'peoples', 'inspection')->first();

        $peoples = $planification->peoples()->get();
        $selected = [];

        foreach ($peoples as $people) {
            $peopleType = PeopleType::where('id', $people->pivot->people_type_id)->first();
            array_push($selected, [
                'peoples' => $people,
                'peopleTypes' => $peopleType,
                'index' => $people->pivot->leader === 1 ? 1 : 0,
            ]);
        }

        return response()->json([
            'planification' => $planification,
            'participants' => $selected
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model\Planification  $planification
     * @return \Illuminate\Http\Response
     */
    public function edit(Planification $planification)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\Planification  $planification
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Planification $planification)
    {
        try{
            Planification::whereId($planification->id)->update([
                'construction_id' => $request->construction_id,
                'protocol_id' => $request->protocol_id,
                'description' => $request->description,
                'start_at' => $request->start_date . ' '. $request->start_hour,
                'end_at' => $request->end_date . ' '. $request->end_hour,
            ]);

            $construction = Construction::findOrFail($request->construction_id);
            $construction->update([
                'hasPlanification' => 1,
            ]);

            if ($request->selected) {
                $planification->peoples()->detach();

                for ($i=0; $i < count($request->selected); $i++) {
                    $planification->peoples()->attach($request->selected[$i]['peoples']['id'], [
                        'people_type_id' => $request->selected[$i]['peopleTypes']['id'],
                        'leader' => $request->leader === $request->selected[$i]['peoples']['id'] ? 1 : 0,
                    ]);
                }
            }

            return response()->json([
                'message' => 'success',
                'id' => $planification->id
            ], 200);

        }catch(\Exception $e){
            return response()->json([
                'message' => $e->getMessage(),
            ], 400);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\Planification  $planification
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $planification = Planification::findOrFail($id);
        $planification->delete();

        return response()->json([
            'message' => 'success',
        ], 200);
    }
}
