<?php

namespace App\Http\Controllers;

use App\Model\Department;
use Illuminate\Http\Request;

class DepartmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $departments = Department::with('provinces')->get();

        return response()->json([
            'departments' => $departments
        ], 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        try {
            $department = Department::create($request->all());

            return response()->json([
                'message' => 'success',
                'id' => $department->id,
            ], 200);

        }catch (\Exception $e){

            return response()->json([
                'message' => $e->getMessage(),
            ], 400);
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\Department  $department
     * @return \Illuminate\Http\Response
     */
    public function show(Department $department)
    {
        try {

            $department = Department::where('id', $department->id)->with('provinces', 'provinces.districts')->first();

            return response()->json([
                'department' => $department,
            ], 200);

        }catch (\Exception $e){

            return response()->json([
                'message' => $e->getMessage(),
            ], 404);

        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model\Department  $department
     * @return \Illuminate\Http\Response
     */
    public function edit(Department $department)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\Department  $department
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Department $department)
    {
        try{

            $department->update($request->all());

            return response()->json([
                'message' => 'Success',
                'id' => $department->id
            ], 200);

        }catch(\Exception $e) {

            return response()->json([
                'message' => $e->getMessage(),
            ], 400);

        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\Department  $department
     * @return \Illuminate\Http\Response
     */
    public function destroy(Department $department)
    {
        $department->delete();

        return response()->json([
            'message' => 'Successfully'
        ], 200);
    }
}
