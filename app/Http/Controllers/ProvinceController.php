<?php

namespace App\Http\Controllers;

use App\Model\Province;
use Illuminate\Http\Request;

class ProvinceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $provinces = Province::with('department', 'districts')->get();

        return response()->json([
            'provinces' => $provinces,
        ], 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $province = Province::create($request->all());

            return response()->json([
                'message' => 'success',
                'id' => $province->id,
            ], 200);

        }catch (\Exception $e){

            return response()->json([
                'message' => $e->getMessage(),
            ], 400);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\Province  $province
     * @return \Illuminate\Http\Response
     */
    public function show(Province $province)
    {
        try {

            $province = Province::where('id', $province->id)->with('department', 'districts')->first();

            return response()->json([
                'province' => $province,
            ], 200);

        }catch (\Exception $e){

            return response()->json([
                'message' => $e->getMessage(),
            ], 404);

        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model\Province  $province
     * @return \Illuminate\Http\Response
     */
    public function edit(Province $province)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\Province  $province
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Province $province)
    {
        try{

            $province->update($request->all());

            return response()->json([
                'message' => 'Success',
                'id' => $province->id
            ], 200);

        }catch(\Exception $e) {

            return response()->json([
                'message' => $e->getMessage(),
            ], 400);

        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\Province  $province
     * @return \Illuminate\Http\Response
     */
    public function destroy(Province $province)
    {
        $province->delete();

        return response()->json([
            'message' => 'Successfully'
        ], 200);
    }
}
