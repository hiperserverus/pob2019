<?php

namespace App\Http\Controllers;

use App\Model\Observation;
use Illuminate\Http\Request;

class ObservationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $observations = Observation::with('protocol', 'observation_type', 'evaluations')->get();

        return response()->json(['observations' => $observations]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            Observation::create($request->all());

            return response()->json(['message' => 'success'], 200);

        }catch(\Exception $e){
            return response()->json([
                'message' => $e->getMessage(),
            ], 400);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\Observation  $observation
     * @return \Illuminate\Http\Response
     */
    public function show(Observation $observation)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model\Observation  $observation
     * @return \Illuminate\Http\Response
     */
    public function edit(Observation $observation)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\Observation  $observation
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try{
            $observation = Observation::findOrFail($id);
            $observation->update($request->all());

            return response()->json(['message' => 'success'], 200);

        }catch(\Exception $e){
            return response()->json([
                'message' => $e->getMessage(),
            ], 400);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\Observation  $observation
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $observation = Observation::findOrFail($id);
        $observation->delete();

        return response()->json(['message' => 'success'], 200);
    }
}
