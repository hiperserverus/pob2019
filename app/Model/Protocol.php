<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Protocol extends Model
{
    protected $fillable = [
        'name', 'status'
    ];
		
    public function observations()
    {
        return $this->hasMany('App\Model\Observation');
    }

    public function planifications()
    {
        return $this->hasMany('App\Model\Planification');
    }
}
