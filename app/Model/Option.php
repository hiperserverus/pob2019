<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Option extends Model
{
    protected $fillable = [
        'description', 'status'
    ];

    public function evaluations()
    {
        return $this->belongsToMany('App\Model\Evaluation', 'evaluation_options', 'option_id', 'evaluation_id');
    }

    public function reports()
    {
        return $this->hasMany('App\Model\Report');
    }
}
