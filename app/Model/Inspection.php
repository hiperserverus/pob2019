<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Inspection extends Model
{
    protected $fillable = [
        'planification_id', 'latitude', 'longitude', 'distance', 'time', 'total', 'status'
    ];

    public function reports()
    {
        return $this->hasMany('App\Model\Report');
    }

    public function planification()
    {
        return $this->belongsTo('App\Model\Planification');
    }

    public function peoples()
    {
        return $this->belongsToMany('App\Model\People', 'report_participants', 'inspection_id', 'people_id')->withPivot('people_type_id', 'leader');
    }
}
