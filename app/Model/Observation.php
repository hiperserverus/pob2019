<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Observation extends Model
{
    protected $fillable = [
        'protocol_id', 'observation_type_id', 'description', 'short_description'
    ];

    public function protocol()
    {
        return $this->belongsTo('App\Model\Protocol');
    }

    public function observation_type()
    {
        return $this->belongsTo('App\Model\ObservationType');
    }

    public function evaluations()
    {
        return $this->hasMany('App\Model\Evaluation');
    }

    public function reports()
    {
        return $this->hasMany('App\Model\Report');
    }
}
