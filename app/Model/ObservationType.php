<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ObservationType extends Model
{
    protected $fillable = [
        'name'
    ];

    public function observations()
    {
        return $this->hasMany('App\Model\Observation');
    }
}
