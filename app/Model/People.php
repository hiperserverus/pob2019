<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class People extends Model
{
    protected $fillable = [
        'enterprise_id',
        'user_id',
        'first_name',
        'last_name',
        'email',
        'phone',
        'address',
        'rut',
        'birthdate',
        'type',
        'status',
        'photo'
    ];

    /* Relaciones */

    public function enterprise()
    {
        return $this->belongsTo('App\Model\Enterprise');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function peopleTypes()
    {
        return $this->BelongsToMany('App\Model\PeopleType', 'people_roles', 'people_id', 'people_type_id');
    }

    public function constructions()
    {
        return $this->belongsToMany('App\Model\Construction', 'construction_people', 'people_id', 'construction_id')->withPivot('people_type_id');
    }

    public function planifications()
    {
        return $this->belongsToMany('App\Model\Planification', 'participants', 'people_id', 'planification_id')->withPivot('people_type_id');
    }

    public function inspections()
    {
        return $this->belongsToMany('App\Model\Inspection', 'report_participants', 'people_id', 'inspection_id')->withPivot('people_type_id');
    }
}
