<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class PeopleType extends Model
{
    protected $fillable = [
        'name', 'isSupervisor', 'isInternal'
    ];

    /* Relaciones */

    public function peoples()
    {
        return $this->belongsToMany('App\Model\People', 'people_roles', 'people_type_id', 'people_id');
    }

    public function enterprises()
    {
        return $this->belongsToMany('App\Model\Enterprise', 'enterprise_roles', 'people_type_id', 'enterprise_id');
    }
}
