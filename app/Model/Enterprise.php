<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Enterprise extends Model
{
    protected $fillable =[
        'department_id',
        'province_id',
        'district_id',
        'name',
        'auxiliaryCode',
        'auxiliaryName',
        'origin',
        'auxiliarySorter',
        'documentType',
        'ruc',
        'address',
        'ubigeo',
        'location',
        'contactName',
        'phone',
        'fax',
        'email',
        'status',
    ];

    public function persons(){
        return $this->hasMany('App\Model\People');
    }

    public function constructions()
    {
        return $this->hasMany('App\Model\Construction');
    }

    public function peopleTypes()
    {
        return $this->BelongsToMany('App\Model\PeopleType', 'enterprise_roles', 'enterprise_id', 'people_type_id');
    }

    public function department()
    {
        return $this->belongsTo('App\Model\Department');
    }

    public function province()
    {
        return $this->belongsTo('App\Model\Province');
    }

    public function district()
    {
        return $this->belongsTo('App\Model\District');
    }
}
