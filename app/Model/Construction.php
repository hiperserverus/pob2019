<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Construction extends Model
{
    protected $fillable = [
        'auxiliaryCode', 'auxiliaryConstruction', 'enterprise_id', 'department_id', 'province_id', 'district_id', 'name', 'ubigeo', 'description', 'address', 'latitude', 'longitude', 'start_date', 'end_date', 'status', 'zip_code', 'type'
    ];

    public function peoples()
    {
        return $this->belongsToMany('App\Model\People', 'construction_people', 'construction_id', 'people_id')->withPivot('people_type_id', 'leader');
    }

    public function planifications()
    {
        return $this->hasMany('App\Model\Planification');
    }

    public function enterprise()
    {
        return $this->belongsTo('App\Model\Enterprise');
    }

    public function department()
    {
        return $this->belongsTo('App\Model\Department');
    }

    public function province()
    {
        return $this->belongsTo('App\Model\Province');
    }

    public function district()
    {
        return $this->belongsTo('App\Model\District');
    }
}
