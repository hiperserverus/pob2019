<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Planification extends Model
{
    protected $fillable = [
        'construction_id', 'protocol_id', 'description', 'start_at', 'end_at', 'status', 'type'
    ];

    public function construction()
    {
        return $this->belongsTo('App\Model\Construction');
    }

    public function protocol()
    {
        return $this->belongsTo('App\Model\Protocol');
    }

    public function peoples()
    {
        return $this->belongsToMany('App\Model\People', 'participants', 'planification_id', 'people_id')->withPivot('people_type_id', 'leader');
    }

    public function inspection()
    {
        return $this->hasOne('App\Model\Inspection');
    }
}
