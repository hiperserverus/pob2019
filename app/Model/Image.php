<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    protected $fillable = ['path'];

    public function reports()
    {
        return $this->belongsToMany('App\Model\Report', 'report_images', 'image_id', 'report_id');
    }
}
