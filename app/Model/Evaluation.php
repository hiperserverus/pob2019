<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Evaluation extends Model
{
    protected $fillable =[
        'observation_id', 'description', 'short_description', 'status', 'percentage'
    ];

    public function observation()
    {
        return $this->belongsTo('App\Model\Observation');
    }

    public function options()
    {
        return $this->belongsToMany('App\Model\Option', 'evaluation_options', 'evaluation_id', 'option_id')->withPivot('percentage', 'defaultOption');
    }

    public function reports()
    {
        return $this->hasMany('App\Model\Report');
    }
}
