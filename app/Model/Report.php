<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Report extends Model
{
    protected $fillable = [
        'inspection_id', 'observation_id', 'evaluation_id', 'option_id', 'comment', 'urlImage', 'urlVideo', 'evaluation_percentage', 'evaluation_option_percentage'
    ];

    public function inspection()
    {
        return $this->belongsTo('App\Model\Inspection');
    }

    public function observation()
    {
        return $this->belongsTo('App\Model\Observation');
    }

    public function evaluation()
    {
        return $this->belongsTo('App\Model\Evaluation');
    }

    public function option()
    {
        return $this->belongsTo('App\Model\Option');
    }

    public function images()
    {
        return $this->belongsToMany('App\Model\Image', 'report_images', 'report_id', 'image_id');
    }
}
