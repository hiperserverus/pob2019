<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Province extends Model
{
    protected $fillable = [
        'name', 'code', 'department_id'
    ];

    public function department()
    {
        return $this->belongsTo('App\Model\Department');
    }

    public function districts()
    {
        return $this->hasMany('App\Model\District');
    }
}
