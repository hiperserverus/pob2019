<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class District extends Model
{
    protected $fillable = [
        'name', 'code', 'province_id'
    ];

    public function province()
    {
        return $this->belongsTo('App\Model\Province');
    }
}
